__author__ = 'Francesco'

class Ambient:

    def __init__(self, color):

        """
        This class models the Ambient object. The only parameter needed is the
        light color to use.
        :param color: a List containing the r, g, b values of the color to use
        :return: an Ambient object instance
        """

        self.color = color




