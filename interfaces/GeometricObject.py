__author__ = 'Francesco'
from numpy import random
class GeometricObject(object):

    ''' Interfaccia per poter interagire con i diversi tipi di oggetti che un raggio puo' trovare nella scena
    '''
    def __init__(self):
        '''inizializzazione'''
        self.isLight = False
        self.light_intensity = 1.0
        self.id = hash(random.sample())

    def intersect(self, ray):
        '''intersezione raggio-oggetto'''

    def normal(self, pt):
        '''metodo che ritorna la normale dell'oggetto corrente nel punto specificato in ingresso'''
