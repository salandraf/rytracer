__author__ = 'Francesco'


from cv2 import imread, split, merge
from random import uniform
from pylab import imshow, pause, ion, show, imsave, axis
from numpy import zeros, float32, clip, sqrt, random
from basic_elements.Point import Point
from basic_elements.Vector import Vector
from Camera import Camera
from Texture import Texture
from BumpMap import BumpMap
from Ray import Ray
from materials.Material import Material
from primitives.Sphere import Sphere
from primitives.Cube import Cube
from primitives.Plane import Plane
from Viewplane import Viewplane
from lights.Ambient import Ambient
from Shader import Shader
import RyGUI as gui

def setScene():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(-7, 10, -7)
    lookat = Point(0, 0, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 8
    f_number = 5
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.3, 0.0, 0.6, 0.8, 10, 0.0, 0.9, 5, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 100.0], None, None)
    white_matte = Material(0.1, 0.0, 0.1, 0.0, 50, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [90, 90, 90], None, None)
    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [255, 50, 50], None, None)

    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([5, 5, 5])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True

    #lights = [sl]
    lights = []

    s4 = Sphere(Point(5, 0.5, 8), 0.5, light_material)
    bottom = Plane(Point(-10, 0, 20), Point(10, 0, 20), Point(-10, 0, -20), Point(10, 0, -20), white_matte, False)
    cube = Cube(blue_matte, True, 0, 0, 0, 6, 6, 6)

    '''SCENE Settigs'''
    scene = [bottom, s4, cube]

    return scene, lights, ambient, camera


def set_scene_2():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 6
    f_number = 5
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''BUMP MAPS Settings'''
    face_bmap = BumpMap('bump_maps/face_norm.jpg', 10, 3)

    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 70.0], None, None)
    green_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.0, [0.0, 70.0, 0.0], None, None)
    red_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.5, [70.0, 0.0, 0.0], None, None)
    white_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [70, 70, 70], None, None)
    yellow_matte = Material(0.1, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [50.0, 50.0, 0.0], None, None)
    violet_plastic = Material(0.1, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [50.0, 0.0, 50.0], None, None)

    metal = Material(0.01, 0.0, 0.5, 0.5, 100, 0.0, 0.0, 5, 1.0, 0.0, 20, 0.0, [9, 9, 9], None, face_bmap)
    glass = Material(0.01, 0.0, 0.0, 1.0, 100, 1.0, 0.0, 5, 0.0, 0.0, 20, 1.9, [0.0, 0.0, 0.0], None, None)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)

    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([5, 5, 5])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True


    metal_s = Sphere(Point(0, 2, 3.9), 1, metal)
    glass_s = Sphere(Point(1, 3, -1.5), 1.5, glass)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)

    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_matte, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_matte, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), blue_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), red_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), green_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5), white_matte, True)

    cube = Cube(blue_matte, False, -4.9, 0, 2.9, 2, 2, 2)
    plane_light1 = Plane(Point(1, 9.8, 1), Point(-1, 9.8, 1), Point(1, 9.8, -1), Point(-1, 9.8, -1), light_material, True)
    plane_light1.isLight=True

    '''SCENE Settigs'''
    scene = [bottom, top, left, right, back, far, plane_light1, cube, metal_s, glass_s, yellow_matte_s, violet_plastic_s]

    return scene, ambient, camera


def set_scene_3():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 8
    f_number = 2.8
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''BUMP MAPS Settings'''
    face_bmap = BumpMap('bump_maps/face_norm.jpg', 10, 3)

    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 30.0], None, None)
    green_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.0, [0.0, 30.0, 0.0], None, None)
    red_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.5, [30.0, 0.0, 0.0], None, None)
    white_glossy = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [30, 30, 30], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [50.0, 50.0, 0.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [50.0, 0.0, 50.0], None, None)

    metal = Material(0.01, 0.0, 0.5, 0.5, 100, 0.0, 0.0, 5, 1.0, 0.0, 20, 0.0, [9, 9, 9], None, face_bmap)
    glass = Material(0.5, 0.0, 0.0, 1.0, 100, 1.0, 0.0, 5, 0.05, 0.0, 20, 1.9, [255, 255, 255], None, None)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)
    blue_light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [55, 55, 255], None, None)

    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True


    metal_s = Sphere(Point(0, 2, 3.9), 1, metal)
    glass_s = Sphere(Point(1, 3, -1.5), 1.5, glass)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)
    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_glossy, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_glossy, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), blue_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), red_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), green_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5),white_glossy, True)

    cube = Cube(blue_light_material, True, -4.9, 0.1, 2.9, 2, 2, 2)
    cube.light_intensity = 0.5
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [bottom, top, left, right, back, far, cube, metal_s, glass_s, yellow_matte_s, violet_plastic_s]

    return scene, ambient, camera

def set_scene_4():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 8
    f_number = 2.8
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''BUMP MAPS Settings'''
    face_bmap = BumpMap('bump_maps/face_norm.jpg', 10, 3)

    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    white_glossy = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [30, 30, 30], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [50.0, 50.0, 0.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [50.0, 0.0, 50.0], None, None)

    metal = Material(0.01, 0.0, 0.5, 0.5, 100, 0.0, 0.0, 10, 1.0, 0.0, 20, 0.0, [9, 9, 9], None, face_bmap)
    glass = Material(0.5, 0.0, 0.0, 1.0, 100, 1.0, 0.0, 10, 0.05, 0.0, 20, 1.9, [255, 255, 255], None, face_bmap)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)
    blue_light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [55, 55, 255], None, None)

    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True


    metal_s = Sphere(Point(0, 2, 3.9), 1, metal)
    glass_s = Sphere(Point(1, 3, -1.5), 1.5, glass)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_glossy, False)

    cube = Cube(blue_light_material, True, -4.9, 0.1, 2.9, 2, 2, 2)
    cube.light_intensity = 0.5
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [bottom, cube, metal_s, glass_s, yellow_matte_s, violet_plastic_s]

    return scene, ambient, camera


def set_scene_5():

    global skybox
    skybox = 'skyboxes\grimmnight_lowres.jpg'

    ''''''
    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 8
    f_number = 2.8
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''BUMP MAPS Settings'''
    earth_bmap = BumpMap('bump_maps/EarthLowRes-bump.jpg', 1.0, 1)
    quake_ground_bmap = BumpMap('bump_maps/shot04-bump.jpg', 0.1, 3)
    stone_wall_bmap = BumpMap('bump_maps/stone_wall_normal_map__.jpg', 4, 1)

    '''TEXTURE Settings'''
    wb_chkbrd = Texture('textures/scacchi.jpg', 0.8, 1)
    earth_tex = Texture('textures/EarthLowRes.jpg', 0.5, 1)
    quake_ground = Texture('textures/shot04.jpg', 1, 1)
    swall = Texture('textures/stone_wall__.jpg', 0.5, 1)


    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    earth = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.5, [30.0, 30.0, 30.0], earth_tex, earth_bmap)
    white_glossy = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [30, 30, 30], swall, stone_wall_bmap)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [50.0, 50.0, 0.0], quake_ground, quake_ground_bmap)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)
    checkerboard = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [55, 55, 255], wb_chkbrd, None)

    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(-55, 31, -22), 1, light_material)
    sl.isLight = True

    earth_sphere = Sphere(Point(-69, 0.0, 60), 40, earth)


    ground = Cube(white_glossy, False, -20.0, 0.1, -10.0, 20, 1.5, 20)
    ground.top.material = checkerboard

    cube1 = Cube(yellow_matte, False, -1.0, 1.1, -1.75, 2, 0.5, 3.5)
    cube2 = Cube(yellow_matte, False, -1.0, 0.6, -1.75, 2.5, 0.5, 3.5)
    cube3 = Cube(yellow_matte, False, -1.0, 0.1, -1.75, 3, 0.5, 3.5)
    stairs = [cube1, cube2, cube3]

    left_side = Cube(white_glossy, False, 0.0, 0.1, 1.75, 2, 2.0, 0.5)
    right_side = Cube(white_glossy, False, 0.0, 0.1, -2.25, 2, 2.0, 0.5)

    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [earth_sphere, ground, left_side, right_side, sl]+stairs

    return scene, ambient, camera


def set_scene_6():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 8
    f_number = 2.8
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''BUMP MAP SETTINGS'''
    gloss_bmap = BumpMap('bump_maps/noisy_map.jpg',1,1)

    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 30.0], None, None)
    green_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.0, [0.0, 30.0, 0.0], None, None)
    red_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.5, [30.0, 0.0, 0.0], None, None)
    white_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 0.0, [30, 30, 30], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [50.0, 50.0, 0.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [50.0, 0.0, 50.0], None, None)

    glossy_metal = Material(0.01, 0.0, 0.1, 0.5, 100, 0.0, 0.0, 10, 0.0, 1.0, 20, 0.0, [9, 9, 9], None, None)
    glossy_glass = Material(0.5, 0.0, 0.0, 1.0, 100, 0.9, 0.0, 5, 0.0, 1.0, 20, 1.9, [255, 255, 255], None, None)
    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)


    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    metal_s = Sphere(Point(0, 2, 3.9), 1, glossy_metal)
    glass_s = Sphere(Point(1, 3, -1.5), 1.5, glossy_glass)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)
    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_matte, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_matte, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), blue_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), red_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), green_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5),white_matte, True)
    cube = Cube(blue_matte, False, -4.9, 0.1, 2.9, 2, 2, 2)



    '''SCENE Settigs'''
    scene = [back, far, bottom, top, left, right, cube, metal_s, glass_s, yellow_matte_s, violet_plastic_s, plane_light1]

    return scene, ambient, camera


def set_scene_7():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 8
    f_number = 2.8
    camera = Camera(pov, lookat, up, focal_lenght, f_number)

    '''BUMP MAPS Settings'''
    stone_wall_bmap = BumpMap('bump_maps/stone_blocks_500.jpg', 4, 1)
    marble_tiles_bmap = BumpMap('bump_maps/marble_texture1821_500.jpg', 0.5, 1)

    '''TEXTURE Settings'''
    marble_tiles_tex = Texture('textures/marble_texture1821_500.jpg', 1, 1)
    stone_wall_tex = Texture('textures/stone_blocks_500.jpg', 0.8, 1)



    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 70.0], None, None)
    marble = Material(0.01, 0.0, 0.2, 0.4, 10, 0.0, 0.0, 5, 0.4, 0.0, 20, 0.0, [70, 70, 70], marble_tiles_tex, marble_tiles_bmap)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 70.0, 0.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 0.0, 70.0], None, None)

    metal = Material(0.01, 0.0, 0.1, 0.5, 100, 0.0, 0.0, 10, 1.0, 0.0, 20, 0.0, [9, 9, 9], None, None)
    glass = Material(0.5, 0.0, 0.0, 1.0, 100, 0.9, 0.0, 10, 0.00, 0.0, 20, 1.9, [255, 255, 255], None, None)

    stone_wall = Material(0.5, 0.0, 0.2, 0.1, 10, 0.0, 0.0, 5, 0.0, 0.0, 20, 1.0, [70, 70, 70], stone_wall_tex, stone_wall_bmap)
    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)


    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True

    metal_s = Sphere(Point(0, 2, 3.9), 1, metal)
    glass_s = Sphere(Point(1, 3, -1.5), 1.5, glass)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)

    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), stone_wall, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), marble, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), stone_wall, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), stone_wall, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), stone_wall, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5), stone_wall, True)

    cube = Cube(blue_matte, False, -4.9, 0.1, 2.9, 2, 2, 2)
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [back, far, bottom, top, left, right, cube, metal_s, glass_s, yellow_matte_s, violet_plastic_s, plane_light1]

    return scene, ambient, camera

def set_scene_8():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 3
    f_number = 3.3
    camera = Camera(pov, lookat, up, focal_lenght, f_number)


    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 70.0], None, None)
    red_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 0.0, 0.0], None, None)
    green_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 70.0, 0.0], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 70.0, 0.0], None, None)
    white_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 70.0, 70.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 0.0, 70.0], None, None)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)


    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True

    red_s = Sphere(Point(0, 2, 3.9), 1, red_matte)
    green_s = Sphere(Point(1, 3, -1.5), 1.5, green_matte)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)

    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_matte, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_matte, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), white_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), white_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), white_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5), white_matte, True)

    cube = Cube(blue_matte, False, -4.9, 0.1, 2.9, 2, 2, 2)
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [back, far, bottom, top, left, right, cube, red_s, green_s, yellow_matte_s, violet_plastic_s, plane_light1]

    return scene, ambient, camera


def set_scene_9():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 3
    f_number = 3.3
    camera = Camera(pov, lookat, up, focal_lenght, f_number)


    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 70.0], None, None)
    red_glossy = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.9, 20, 1.33, [70.0, 0.0, 0.0], None, None)
    green_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 70.0, 0.0], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 70.0, 0.0], None, None)
    white_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 70.0, 70.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 0.0, 70.0], None, None)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)


    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True

    red_s = Sphere(Point(0, 2, 3.9), 1, red_glossy)
    green_s = Sphere(Point(1, 3, -1.5), 1.5, green_matte)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)

    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_matte, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_matte, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), white_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), white_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), white_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5), white_matte, True)

    cube = Cube(blue_matte, False, -4.9, 0.1, 2.9, 2, 2, 2)
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [back, far, bottom, top, left, right, cube, red_s, green_s, yellow_matte_s, violet_plastic_s, plane_light1]

    return scene, ambient, camera


def set_scene_10():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(4.5, 3, 0)
    lookat = Point(0, 3, 0)
    up = Vector(0, 1, 0)
    focal_lenght = 3
    f_number = 3.3
    camera = Camera(pov, lookat, up, focal_lenght, f_number)


    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 70.0], None, None)
    red_glossy = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 1.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 0.0, 0.0], None, None)
    green_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 70.0, 0.0], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 70.0, 0.0], None, None)
    white_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 70.0, 70.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 0.0, 70.0], None, None)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)


    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True

    red_s = Sphere(Point(0, 2, 3.9), 1, red_glossy)
    green_s = Sphere(Point(1, 3, -1.5), 1.5, green_matte)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)

    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_matte, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_matte, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), white_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), white_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), white_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5), white_matte, True)

    cube = Cube(blue_matte, False, -4.9, 0.1, 2.9, 2, 2, 2)
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [back, far, bottom, top, left, right, cube, red_s, green_s, yellow_matte_s, violet_plastic_s, plane_light1]

    return scene, ambient, camera

def set_scene_11():

    global skybox
    skybox = None

    '''CAMERA Settings'''
    pov = Point(-4.5, 3, 0)
    lookat = Point(0, 3, 4)
    up = Vector(0, 1, 0)
    focal_lenght = 3
    f_number = 3.3
    camera = Camera(pov, lookat, up, focal_lenght, f_number)


    '''MATERIALS Settings (ka,  ke,  kd,  ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, [r.r, g.g, b.b], texture, bump_map)'''
    blue_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 0.0, 70.0], None, None)
    red_glossy = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 1.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 0.0, 0.0], None, None)
    green_matte = Material(0.01, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [0.0, 70.0, 0.0], None, None)
    yellow_matte = Material(0.01, 0.0, 0.9, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 70.0, 0.0], None, None)
    white_matte = Material(0.1, 0.0, 0.5, 0.0, 10, 0.0, 0.0, 10, 0.0, 0.0, 20, 1.33, [70.0, 70.0, 70.0], None, None)
    violet_plastic = Material(0.01, 0.0, 0.9, 0.7, 50, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [70.0, 0.0, 70.0], None, None)

    light_material = Material(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, 0.0, 0.0, 20, 0.0, [255, 255, 255], None, None)


    '''OBJECTS AND LIGHTS Settings'''
    ambient = Ambient([0.1, 0.1, 0.1])
    sl = Sphere(Point(15, 15, 15), 1, light_material)
    sl.isLight = True

    red_s = Sphere(Point(0, 2, 3.9), 1, red_glossy)
    green_s = Sphere(Point(1, 3, -1.5), 1.5, green_matte)
    yellow_matte_s = Sphere(Point(-2.5, 8, 0), 0.5, yellow_matte)
    violet_plastic_s = Sphere(Point(-3, 3, 3.9), 1, violet_plastic)

    top = Plane(Point(-5, 10, 5), Point(5, 10, 5), Point(-5, 10, -5), Point(5, 10, -5), white_matte, True)
    bottom = Plane(Point(-5, 0, 5), Point(5, 0, 5), Point(-5, 0, -5), Point(5, 0, -5), white_matte, False)
    far = Plane(Point(-5, 10, 5), Point(-5, 0, 5), Point(-5, 10, -5), Point(-5, 0, -5), white_matte, False)
    left = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(-5, 10, 5), Point(-5, 0, 5), white_matte, False)
    right = Plane(Point(-5, 10, -5), Point(-5, 0, -5), Point(5, 10, -5), Point(5, 0, -5), white_matte, False)
    back = Plane(Point(5, 10, 5), Point(5, 0, 5), Point(5, 10, -5), Point(5, 0, -5), white_matte, True)

    cube = Cube(blue_matte, False, -4.9, 0.1, 2.9, 2, 2, 2)
    plane_light1 = Plane(Point(1, 9.9, 1), Point(-1, 9.9, 1), Point(1, 9.9, -1), Point(-1, 9.9, -1), light_material, False)
    plane_light1.isLight = True
    plane_light1.light_intensity = 1.0

    '''SCENE Settigs'''
    scene = [back, far, bottom, top, left, right, cube, red_s, green_s, yellow_matte_s, violet_plastic_s, plane_light1]

    return scene, ambient, camera

def start_rendering(antialiasing_on, soft_shadows_on, jittering_on, depth_of_field_on, scene, ambient, camera, viewplane):

    """
    This function starts the rendering of the scene based upon the parameters specified:
    :param antialiasing_on: a boolean value which triggers the usage of antialiasing feature
    :param soft_shadows_on: a boolean value which triggers the modeling of soft shadows into the scene
    :param jittering_on: a boolean value which triggers the formation of the scene casting random rays towards the viewplane
    :param depth_of_field_on: a boolean value which enable the usage of the depth of field feature
    """
    raw_scene = elaborate_scene(scene)

    '''Creating and saving a black image for the subsequent loading into the UI window'''
    img = zeros((viewplane.height, viewplane.width), dtype=float32)
    imsave('test.png', img)
    img = imread('test.png')
    b, g, r = split(img)
    ion()
    image = imshow(img, animated=True)
    axis('off')

    '''The jittering is the first feature with an impact on the way image is rendered'''
    if jittering_on:
        img = jitter(image, r, g, b, depth_of_field_on,soft_shadows_on, scene, ambient, camera, viewplane)

    else:
        '''Then we manage the antialiasing feature'''
        if antialiasing_on:
            img = antialiasing(image, r, g, b, scene, ambient, camera, viewplane)

        else:
            '''if we do not use antialiasing we must check for the use of depth of field and soft shadows
            since soft shadows are triggered using a flag directly onto the shader, we split the computation
            flow by checking depth of field usage first'''
            if depth_of_field_on:
                img = depth_of_field(image, r, g, b, soft_shadows_on, scene, ambient, camera, viewplane)

            else:
                '''we are not using anything else apart for the soft shadows, eventually'''
                ray = Ray(camera.eye, camera.w)
                shader = Shader(ray, scene, ambient, camera, level_of_recursion, soft_shadows_on, soft_shadow_samples, skybox)

                for v in xrange(int(viewplane.height)):
                    for u in xrange(int(viewplane.width)):

                        px = camera.eye + camera.u * (u + 0.5 - viewplane.width*0.5) + camera.v * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
                        #dist = sqrt((px-camera.eye).dot(px-camera.eye))# - distance
                        #px = camera.eye + camera.u  * (u + 0.5 - hres*0.5) + camera.v  * ((vres-v) + 0.5 - vres*0.5) + camera.w * dist
                        d = (px - camera.eye).normalize()
                        ray.d = d
                        ray.intersect(raw_scene)
                        shader.ray = ray
                        shader.shade()
                        r[v][u] = clip(ray.color[0], 0, 255)
                        g[v][u] = clip(ray.color[1], 0, 255)
                        b[v][u] = clip(ray.color[2], 0, 255)
                    '''updating image pixels'''
                    img = merge((r, g, b))
                    image.set_data(img)
                    pause(0.0000001)
    show(block=True)
    imsave('test.png', img)


def elaborate_scene(scene):

    """
     This function is used to decompose complex objects in the scene in simpler ones
     (es. Cube is decomposed into its planes)
    :param scene:
    :return raw_scene: the scene decomposed
    """
    raw_scene = []
    for obj in scene:
        if hasattr(obj, 'faces'):
            faces=getattr(obj, 'faces')
            for face in faces:
                raw_scene.append(face)
        else:
            raw_scene.append(obj)
    return raw_scene


def depth_of_field(image, r, g, b, soft_shadows_on, scene, ambient, camera, viewplane):

    """
    :param image: the object linked to the UI window showing the rendering update
    :param r:
    :param g:
    :param b:
    :param soft_shadows_on: boolean which enables the modeling of soft shadows into the scene
    :param scene:
    :param ambient:
    :param camera:
    :param viewplane:
    :return: the matrix of pixels which constitutes the image to be saved
    """

    img = None
    raw_scene = elaborate_scene(scene)
    dof_ray = Ray(camera.eye, camera.w)
    shader = Shader(dof_ray, scene, ambient, camera, level_of_recursion, soft_shadows_on, soft_shadow_samples, skybox)

    samples = depth_of_field_samples

    f = camera.focal_length
    fnumber = camera.f_number
    aperture = f/fnumber
    half_aperture = aperture*0.5


    for v in xrange(int(viewplane.height)):
        for u in xrange(int(viewplane.width)):

            '''This is needed to avoid unintended zooming caused by moving the focus plane'''
            coeff = f/float(viewplane.distance)

            px = camera.eye + camera.u *coeff * (u + 0.5 - viewplane.width*0.5) + camera.v *coeff * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
            dist = sqrt((px-camera.eye).dot(px-camera.eye))# - distance
            px = camera.eye + camera.u *coeff * (u + 0.5 - viewplane.width*0.5) + camera.v *coeff * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * dist
            fp = px + camera.w * -(f+dist)
            d = (px - camera.eye).normalize()
            color = [0, 0, 0]

            for i in xrange(samples):

                uoffset = uniform(-half_aperture, half_aperture)
                voffset = uniform(-half_aperture, half_aperture)
                lensp = camera.eye + camera.u * uoffset + camera.v * voffset
                d = (fp - lensp).normalize()

                dof_ray.o = lensp
                dof_ray.d = d
                dof_ray.intersect(raw_scene)
                shader.ray = dof_ray
                shader.shade()

                color = [color[k] + dof_ray.color[k] for k in xrange(len(color))]

            r[v][u] = clip((color[0]/samples), 0, 255)
            g[v][u] = clip((color[1]/samples), 0, 255)
            b[v][u] = clip((color[2]/samples), 0, 255)

        '''updating image pixels'''
        img = merge((r, g, b))
        image.set_data(img)
        pause(0.0000001)

    return img


def jitter(image, r, g, b, depth_of_field_on, soft_shadows_on, scene, ambient, camera, viewplane):

    """
    :param image: the object linked to the UI window showing the rendering update
    :param r:
    :param g:
    :param b:
    :param depth_of_field_on: boolean which enables the simulation of depth of field on the camera
    :param soft_shadows_on: boolean which enables the modeling of soft shadows into the scene
    :param scene:
    :param ambient:
    :param camera:
    :param viewplane:
    :return: the matrix of pixels which constitutes the image to be saved
    """

    img = None
    raw_scene = elaborate_scene(scene)
    points = viewplane.height * viewplane.width * 2

    ray = Ray(camera.eye, camera.w)
    shader = Shader(ray, scene, ambient, camera, level_of_recursion, soft_shadows_on, soft_shadow_samples, skybox)

    for i in xrange(int(points)):

        u = random.sample() * viewplane.width
        v = random.sample() * viewplane.height

        if depth_of_field_on:
            img = depth_of_field(r, g, b, soft_shadows_on, scene, ambient, camera, viewplane, image)


        else:
            px = camera.eye + camera.u * (u + 0.5 - viewplane.width*0.5) + camera.v * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
            d = (px - camera.eye).normalize()
            ray.d = d
            ray.intersect(raw_scene)
            shader.ray = ray
            shader.shade()
            r[v][u] = clip(ray.color[0], 0, 255)
            g[v][u] = clip(ray.color[1], 0, 255)
            b[v][u] = clip(ray.color[2], 0, 255)
            img = merge((r, g, b))
            image.set_data(img)
            pause(0.0000001)

    return img


def antialiasing(image, r, g, b, scene, ambient, camera, viewplane):

    """
    :param image: the object linked to the UI window showing the rendering update
    :param r:
    :param g:
    :param b:
    :param scene:
    :param ambient:
    :param camera:
    :param viewplane:
    :return: the matrix of pixels which constitutes the image to be saved
    """

    img = None
    raw_scene = elaborate_scene(scene)
    ray = Ray(camera.eye, camera.w)
    shader = Shader(ray, scene, ambient, camera, level_of_recursion, soft_shadows_on, soft_shadow_samples, skybox)
    sqrt_samples = sqrt(antialiasing_samples)
    range_max = 1/float(sqrt_samples)
    for v in xrange(int(viewplane.height)):
        for u in xrange(int(viewplane.width)):
            color = [0,0,0]
            px = camera.eye + camera.u * (u + 0.5 - viewplane.width*0.5) + camera.v * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance

            for i in xrange(antialiasing_samples):
                px = px + camera.u * random.uniform(0,range_max) + camera.v * random.uniform(0,range_max)
                d = (px - camera.eye).normalize()
                ray.d = d
                ray.intersect(raw_scene)
                shader.ray = ray
                shader.shade()
                color = [color[k] + 1/float(antialiasing_samples)*ray.color[k] for k in range(len(color))]
            r[v][u] = clip(color[0], 0, 255)
            g[v][u] = clip(color[1], 0, 255)
            b[v][u] = clip(color[2], 0, 255)

        '''updating image pixels'''
        img = merge((r, g, b))
        image.set_data(img)
        pause(0.0000001)
    return img



if __name__ == '__main__':

    '''Viewplane Settings'''
    spx = 1.0
    hres = 220
    aspect = 1.333
    vres = round(hres / aspect)
    distance = vres/2
    viewplane = Viewplane(hres, vres, distance)
    ''''''

    scene, ambient, camera = set_scene_11()

    global antialiasing_samples, soft_shadow_samples, jittering_samples, depth_of_field_samples, skybox, level_of_recursion

    antialiasing_on = False
    antialiasing_samples = 20
    soft_shadows_on = False
    soft_shadow_samples = 20
    jittering_on = False
    jittering_samples = 20
    depth_of_field_on = False
    depth_of_field_samples = 20
    level_of_recursion = 2

    start_rendering(antialiasing_on, soft_shadows_on, jittering_on, depth_of_field_on, scene, ambient, camera, viewplane)


    #gui = gui.RyGUI()
    #gui.setup()





