__author__ = 'Francesco'

from Tkinter import *
from tkColorChooser import askcolor
from tkFileDialog import askopenfilename
from RenderEngine import *
import os


class RyGUI:

    def __init__(self):
        """
        This class models the GUI for the creating a complete scene
        :return: void
        """
        self.objectList = ['Triangle', 'Plane', 'Cube', 'Sphere']
        self.render_engine = RenderEngine()
        self.newObject = {}
        self.materialColor = [0.0, 0.0, 0.0]
        self.ambientColor = [0.0, 0.0, 0.0]
        self.textureFileName=None
        self.bumpMapFileName=None
        self.objectToModify=None
        self.setup()


    def setup(self):

        self.materialColor = [0.0, 0.0, 0.0]

        self.mainmenu = Tk()


        '''''''''''''SCENE'''''''''''''

        self.sceneFrame = LabelFrame(self.mainmenu, text='Scene Settings')
        self.sceneFrame.grid(row=0, column=0, columnspan=1, sticky=NW)

        self.cameraFrame = LabelFrame(self.sceneFrame, text='Camera')
        self.cameraFrame.grid(row=0, column=0, columnspan=1)

        self.labelPov = Label(self.cameraFrame, text='Location Point: <x, y, z>', width=68)
        self.labelPov.grid(row=3, column=0)
        self.xpov = StringVar()
        self.ypov = StringVar()
        self.zpov = StringVar()
        self.xpov.set('1.0')
        self.ypov.set('1.0')
        self.zpov.set('1.0')
        self.xPov = Entry(self.cameraFrame, width=5, textvariable=self.xpov)
        self.xPov.grid(row=3, column=1)
        self.yPov = Entry(self.cameraFrame, width=5, textvariable=self.ypov)
        self.yPov.grid(row=3, column=2)
        self.zPov = Entry(self.cameraFrame, width=5, textvariable=self.zpov)
        self.zPov.grid(row=3, column=3)

        self.labelLookat = Label(self.cameraFrame, text='LookAt Point: <x, y, z>', width=68)
        self.labelLookat.grid(row=4, column=0)
        self.xlat = StringVar()
        self.ylat = StringVar()
        self.zlat = StringVar()
        self.xlat.set('0.0')
        self.ylat.set('0.0')
        self.zlat.set('0.0')
        self.xLookat = Entry(self.cameraFrame, width=5, textvariable=self.xlat)
        self.xLookat.grid(row=4, column=1)
        self.yLookat = Entry(self.cameraFrame, width=5, textvariable=self.ylat)
        self.yLookat.grid(row=4, column=2)
        self.zLookat = Entry(self.cameraFrame, width=5, textvariable=self.zlat)
        self.zLookat.grid(row=4, column=3)

        self.labelUp = Label(self.cameraFrame, text='Up Direction Vector: <x, y, z>', width=68)
        self.labelUp.grid(row=5, column=0)
        self.xup = StringVar()
        self.yup = StringVar()
        self.zup = StringVar()
        self.xup.set('0.0')
        self.yup.set('1.0')
        self.zup.set('0.0')
        self.xUp = Entry(self.cameraFrame, width=5, textvariable=self.xup)
        self.xUp.grid(row=5, column=1)
        self.yUp = Entry(self.cameraFrame, width=5, textvariable=self.yup)
        self.yUp.grid(row=5, column=2)
        self.zUp = Entry(self.cameraFrame, width=5, textvariable=self.zup)
        self.zUp.grid(row=5, column=3)

        self.labelFocal = Label(self.cameraFrame, text='Focal Length: ', width=68)
        self.labelFocal.grid(row=6, column=0)
        self.focal_length = StringVar()
        self.focal_length.set('8.0')
        self.focalField = Entry(self.cameraFrame, width=5, textvariable=self.focal_length)
        self.focalField.grid(row=6, column=1)


        self.labelFnum = Label(self.cameraFrame, text='f-number : ', width=68)
        self.labelFnum.grid(row=7, column=0)
        self.f_number= StringVar()
        self.f_number.set('5.0')
        self.fField = Entry(self.cameraFrame, width=5, textvariable=self.f_number)
        self.fField.grid(row=7, column=1)

        self.viewplaneFrame = LabelFrame(self.sceneFrame, text='View Plane')
        self.viewplaneFrame.grid(row=1, column=0, columnspan=1)

        self.labelHres = Label(self.viewplaneFrame, text='Width: ', width=78)
        self.labelHres.grid(row=0, column=0)
        self.width = StringVar()
        self.width.set('200.0')
        self.widthValue = Entry(self.viewplaneFrame, width=5, textvariable=self.width)
        self.widthValue.grid(row=0, column=1)

        self.labelAspectRatio = Label(self.viewplaneFrame, text='Aspect ratio: ', width=78)
        self.labelAspectRatio.grid(row=1, column=0)
        self.aspect_ratio = StringVar()
        self.aspect_ratio.set('1.0')
        self.aspectRatioValue = Entry(self.viewplaneFrame, width=5, textvariable=self.aspect_ratio)
        self.aspectRatioValue.grid(row=1, column=1)

        self.labelDistance = Label(self.viewplaneFrame, text='Distance: ', width=78)
        self.labelDistance.grid(row=2, column=0)
        self.distanceVar = StringVar()


        self.distanceVar.set('1.0')
        self.distanceEntry = Entry(self.viewplaneFrame, width=5, textvariable=self.distanceVar)
        self.distanceEntry.grid(row=1, column=1)

        '''''''''''''ENVIRONMENT'''''''''''''

        self.environmentFrame = LabelFrame(self.sceneFrame, text='Environment')
        self.environmentFrame.grid(row=2, column=0, columnspan=1, sticky=NW)

        self.ambientLightFrame = LabelFrame(self.environmentFrame, text='Ambient Light')
        self.ambientLightFrame.grid(row=0, column=0, columnspan=2, sticky=NW)

        self.ambientLabel = Label(self.ambientLightFrame, text="Color <r,g,b> :", width=58, height=3)
        self.ambientLabel.grid(row=0, column=0)

        self.rAmbient = StringVar()
        self.gAmbient = StringVar()
        self.bAmbient = StringVar()
        self.rAmbient.set('0.0')
        self.gAmbient.set('0.0')
        self.bAmbient.set('0.0')
        self.rAmbientValue = Entry(self.ambientLightFrame, width=5, textvariable=self.rAmbient)
        self.rAmbientValue.grid(row=0, column=1)
        self.gAmbientValue = Entry(self.ambientLightFrame, width=5, textvariable=self.gAmbient)
        self.gAmbientValue.grid(row=0, column=2)
        self.bAmbientValue = Entry(self.ambientLightFrame, width=5, textvariable=self.bAmbient)
        self.bAmbientValue.grid(row=0, column=3)
        self.selectAmbientColorBtn = Button(self.ambientLightFrame, text='Select color', command=self.on_select_ambient_color_click)
        self.selectAmbientColorBtn.grid(row=0, column=4)

        self.envMapFrame = LabelFrame(self.environmentFrame, text='Environment Mapping (SkyBox)')
        self.envMapFrame.grid(row=1, column=0, columnspan=1)

        self.envLabel = Label(self.envMapFrame, text="Select an image to use as environment mapping:", width=56, height=2)
        self.envLabel.grid(row=0, column=0, sticky=W)

        self.envPathLabel = Label(self.envMapFrame, text="None", width=74, height=3)
        self.envPathLabel.grid(row=1, column=0)

        self.selectEnvMapBtn = Button(self.envMapFrame, text='Browse...', command=self.on_select_environment_click)
        self.selectEnvMapBtn.grid(row=1, column=2)

        self.removeEnvMapBtn = Button(self.envMapFrame, text='Remove', command=self.on_remove_environment_click)
        self.removeEnvMapBtn.grid(row=2, column=2)

        '''''''''''''OBJECTS AND LIGHTS'''''''''''''

        self.objectsAndLightsFrame = LabelFrame(self.mainmenu, text='Objects and Lights')
        self.objectsAndLightsFrame.grid(row=0, column=1, columnspan=1, sticky=NE)

        self.btnAddObject = Button(self.objectsAndLightsFrame, text='Add a New Object', width=80, height=2, command=self.add_object)
        self.btnAddObject.grid(row=1, column=0, columnspan=4)

        self.addedObjectsFrame = LabelFrame(self.objectsAndLightsFrame, text='Added Objects')
        self.addedObjectsFrame.grid(row=2, column=0, columnspan=2)

        self.objectsAdded = Listbox(self.addedObjectsFrame, height=5, width=97)
        self.objectsAdded.grid(row=0, column=0)

        self.btnModifyObject = Button(self.addedObjectsFrame, text='Modify Object', width=80, height=2, command=self.on_modify_object_clicked)
        self.btnModifyObject.grid(row=1, column=0, columnspan=1)

        self.btnRemoveObject = Button(self.addedObjectsFrame, text='Remove Object', width=80, height=2, command=self.on_remove_object_clicked)
        self.btnRemoveObject.grid(row=2, column=0, columnspan=1)

        self.addedLightsFrame = LabelFrame(self.objectsAndLightsFrame, text='Added Lights')
        self.addedLightsFrame.grid(row=3, column=0, columnspan=1)

        self.lightsAdded = Listbox(self.addedLightsFrame, height=5, width=97)
        self.lightsAdded.grid(row=0, column=0)

        self.btnModifyLight = Button(self.addedLightsFrame, text='Modify Light', width=80, height=2, command=self.on_modify_light_clicked)
        self.btnModifyLight.grid(row=1, column=0, columnspan=1)

        self.btnRemoveLight = Button(self.addedLightsFrame, text='Remove Light', width=80, height=2, command=self.on_remove_light_clicked)
        self.btnRemoveLight.grid(row=2, column=0, columnspan=1)


        '''''''''''''OPTIONS'''''''''''''

        self.optionsPanel = LabelFrame(self.mainmenu, text='Additional Features and No.Samples')
        self.optionsPanel.grid(row=2, column=0, columnspan=1, sticky=NW)

        self.dof = BooleanVar()
        self.jittering = BooleanVar()
        self.antialiasing = BooleanVar()
        self.sshadows = BooleanVar()

        self.dof.set('False')
        self.jittering.set('False')
        self.antialiasing.set('False')
        self.sshadows.set('False')

        self.jittering_chkbox = Checkbutton(self.optionsPanel, text="Jittering", variable=self.jittering, onvalue=True, offvalue=False, width=17, anchor=W)
        self.jittering_chkbox.grid(row=0, column=0, columnspan=1, sticky=NW)

        '''self.labelJitteringSamples = Label(self.optionsPanel, text='No. samples: ', width=10, height=2)
        self.labelJitteringSamples.grid(row=1, column=0, sticky=NW)'''
        self.jittering_samples = StringVar()
        self.jittering_samples.set('20')
        self.jitteringSamplesValue = Entry(self.optionsPanel, width=5, textvariable=self.jittering_samples)
        self.jitteringSamplesValue.grid(row=1, column=0)


        self.antialiasing_chkbox = Checkbutton(self.optionsPanel, text="Antialiasing", variable=self.antialiasing, onvalue=True, offvalue=False, width=17, anchor=W)
        self.antialiasing_chkbox.grid(row=0, column=1, columnspan=1, sticky=NW)

        self.antialiasing_samples = StringVar()
        self.antialiasing_samples.set('20')
        self.antialiasingSamplesValue = Entry(self.optionsPanel, width=5, textvariable=self.antialiasing_samples)
        self.antialiasingSamplesValue.grid(row=1, column=1)

        self.dof_chkbox = Checkbutton(self.optionsPanel, text="Depth of Field", variable=self.dof, onvalue=True, offvalue=False, width=17, anchor=W)
        self.dof_chkbox.grid(row=0, column=2, columnspan=1, sticky=NW)

        self.dof_samples = StringVar()
        self.dof_samples.set('20')
        self.dofSamplesValue = Entry(self.optionsPanel, width=5, textvariable=self.dof_samples)
        self.dofSamplesValue.grid(row=1, column=2)


        self.sshadows_chkbox = Checkbutton(self.optionsPanel, text="Soft Shadows", variable=self.sshadows, onvalue=True, offvalue=False, width=17, anchor=W)
        self.sshadows_chkbox.grid(row=0, column=3, columnspan=1, sticky=NW)

        self.sshadows_samples = StringVar()
        self.sshadows_samples.set('20')
        self.sshadowsSamplesValue = Entry(self.optionsPanel, width=5, textvariable=self.sshadows_samples)
        self.sshadowsSamplesValue.grid(row=1, column=3)

        self.labelDepthOfRecursion = Label(self.optionsPanel, text='  ', width=20)
        self.labelDepthOfRecursion.grid(row=2, column=0, columnspan=1)

        self.labelDepthOfRecursion = Label(self.optionsPanel, text='Max depth of recursion: ')
        self.labelDepthOfRecursion.grid(row=3, column=0, columnspan=1)
        self.recursionLevel = StringVar()
        self.recursionLevel.set('2.0')
        self.recursionLevelValue = Entry(self.optionsPanel, width=5, textvariable=self.recursionLevel)
        self.recursionLevelValue.grid(row=3, column=1)

        '''''''''''''START RENDERING'''''''''''''

        self.btnStartRendering = Button(self.mainmenu, text='START RENDERING!', width=85, height=5, command=self.startRendering)
        self.btnStartRendering.grid(row=2, column=1, columnspan=1, rowspan=2, sticky=NW)

        self.mainmenu.mainloop()



    def startRendering(self):

        pov = [float(self.xpov.get()), float(self.ypov.get()), float(self.zpov.get())]
        lookat = [float(self.xlat.get()), float(self.ylat.get()), float(self.zlat.get())]
        up = [float(self.xup.get()), float(self.yup.get()), float(self.zup.get())]
        focal_length = float(self.focal_length.get())
        f_number = float(self.f_number.get())
        self.render_engine.setupCamera(pov, lookat, up, focal_length, f_number)
        hres = float(self.width.get())
        aspect = float(self.aspect_ratio.get())
        self.render_engine.setupViewPlane(hres, aspect)
        self.render_engine.createAmbientLight(self.ambientColor)

        antialiasing = bool(self.antialiasing.get())
        sshadows = bool(self.sshadows.get())
        jittering = bool(self.jittering.get())
        dof = bool(self.dof.get())

        self.render_engine.antialiasing_samples = float(self.antialiasing_samples.get())
        self.render_engine.jittering_samples = float(self.jittering_samples.get())
        self.render_engine.dof_samples = float(self.dof_samples.get())
        self.render_engine.sshadows_samples = float(self.sshadows.get())
        self.render_engine.level_of_recursion = float(self.recursionLevel.get())


        self.render_engine.start_rendering(antialiasing,sshadows,jittering,dof)

    def add_object(self):

        self.add_obj_window = Toplevel(self.mainmenu)
        self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
        self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
        self.objectListBox.bind('<<ListboxSelect>>', self.on_select)

        self.objectListBox.insert(1, self.objectList[0])
        self.objectListBox.insert(2, self.objectList[1])
        self.objectListBox.insert(3, self.objectList[2])
        self.objectListBox.insert(4, self.objectList[3])

        self.objectSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Settings', width=50, height=50)
        self.objectSettingsLabelFrame.grid(row=0, column=1, columnspan=1, sticky=W)
        self.triangleSettings()

        self.materialSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Material', width=50, height=50)
        self.materialSettingsLabelFrame.grid(row=1, column=1, columnspan=1, rowspan=2, sticky=W)
        self.materialSettings()

        self.textureSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Texture', width=50, height=50)
        self.textureSettingsLabelFrame.grid(row=1, column=0, sticky=W)
        self.textureSettings()

        self.bumpMapSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Bump Map', width=50, height=50)
        self.bumpMapSettingsLabelFrame.grid(row=2, column=0, sticky=W)
        self.bumpMapSettings()

        self.add_obj_btn = Button(self.add_obj_window, text='Add', width=10, height=2, command=self.on_add_obj_clicked)
        self.add_obj_btn.grid(row=5, column=0, sticky=W)
        self.close_btn = Button(self.add_obj_window, text='Back', width=10, height=2, command=self.on_back_clicked)
        self.close_btn.grid(row=5, column=1, sticky=E)

    def on_back_clicked(self):
        self.add_obj_window.destroy()

    def on_add_obj_clicked(self):

        bumpmap = {}
        bumpmap['name']=self.bumpMapFileName
        bumpmap['effect_factor']=float(self.effect_factor.get())
        bumpmap['repeatfor']=float(self.repeatforBumpMap.get())

        bumpmapObject = self.render_engine.create_bump_map(bumpmap)


        texture = {}
        texture['name']=self.textureFileName
        texture['attenuation']=float(self.attenuation_factor.get())
        texture['repeatfor']=float(self.repeatforTexture.get())


        textureObject = self.render_engine.create_texture(texture)


        material = {}
        material['ka'] = float(self.ka.get())
        material['ke'] = float(self.ke.get())
        material['kd'] = float(self.kd.get())
        material['ks'] = float(self.ks.get())
        material['sh'] = float(self.sh.get())
        material['kt'] = float(self.kt.get())
        material['kgt'] = float(self.kgt.get())
        material['kgts'] = float(self.kgts.get())
        material['kr'] = float(self.kr.get())
        material['kgr'] = float(self.kgr.get())
        material['kgrs'] = float(self.kgrs.get())
        material['ior'] = float(self.ior.get())
        material['color'] = self.materialColor
        material['texture'] = textureObject
        material['bumpmap']= bumpmapObject

        material = self.render_engine.create_material(material)

        if self.classtype == 'Triangle':

            V0 = [float(self.xV0.get()), float(self.yV0.get()), float(self.zV0.get())]
            V1 = [float(self.xV1.get()), float(self.yV1.get()), float(self.zV1.get())]
            V2 = [float(self.xV2.get()), float(self.yV2.get()), float(self.zV2.get())]

            self.newObject = {'v0': V0, 'v1': V1, 'v2': V2, 'isLight': self.isLight.get()}

        elif self.classtype == 'Plane':

            TL = [float(self.xTL.get()), float(self.yTL.get()), float(self.zTL.get())]
            BL = [float(self.xBL.get()), float(self.yBL.get()), float(self.zBL.get())]
            TR = [float(self.xTR.get()), float(self.yTR.get()), float(self.zTR.get())]
            BR = [float(self.xBR.get()), float(self.yBR.get()), float(self.zBR.get())]

            self.newObject = {'tl': TL, 'bl': BL, 'tr': TR, 'br': BR, 'isLight': self.isLight.get(), 'reverse_normal': self.reverse_normal.get()}


        elif self.classtype == 'Cube':

            dx = float(self.dx.get())
            dy = float(self.dy.get())
            dz = float(self.dz.get())

            sx = float(self.sx.get())
            sy = float(self.sy.get())
            sz = float(self.sz.get())

            self.newObject = {'dx': dx, 'dy': dy, 'dz': dz, 'sx': sx, 'sy': sy, 'sz': sz, 'isLight': self.isLight.get()}

        elif self.classtype == 'Sphere':

            center = [float(self.xc.get()), float(self.yc.get()), float(self.zc.get())]
            radius = float(self.radius.get())

            self.newObject = {'center': center, 'radius': radius, 'isLight': self.isLight.get()}

        self.newObject['material'] = material

        self.render_engine.add_object(self.classtype, self.newObject)
        self.add_obj_window.destroy()
        self.updateObjectsAdded()
        self.updateLightsAdded()
        self.textureFileName = None
        self.bumpMapFileName = None

    def on_modify_object_clicked(self):

        self.add_obj_window = Toplevel(self.mainmenu)


        self.indexInGlobalList = int(self.objectsAdded.curselection()[0])
        self.objectToModify = self.render_engine.objects[self.indexInGlobalList]
        self.newObject = self.objectToModify

        self.objectSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Settings', width=50, height=50)
        self.objectSettingsLabelFrame.grid(row=0, column=1, columnspan=1, sticky=W)
        if isinstance(self.objectToModify, Triangle):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Triangle')
            self.load_triangle_settings(self.objectToModify)

        elif isinstance(self.objectToModify, Plane):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Plane')
            self.load_plane_settings(self.objectToModify)

        elif isinstance(self.objectToModify, Cube):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Cube')
            self.load_cube_settings(self.objectToModify)

        elif isinstance(self.objectToModify, Sphere):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Sphere')
            self.load_sphere_settings(self.objectToModify)

        self.materialSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Material', width=50, height=50)
        self.materialSettingsLabelFrame.grid(row=1, column=1, columnspan=1, rowspan=2, sticky=W)
        self.load_material_settings(self.objectToModify.material)

        self.textureSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Texture', width=50, height=50)
        self.textureSettingsLabelFrame.grid(row=1, column=0, sticky=W)
        self.load_texture_settings(self.objectToModify.material.texture) if self.objectToModify.material.texture != None else self.textureSettings()

        self.bumpMapSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Bump Map', width=50, height=50)
        self.bumpMapSettingsLabelFrame.grid(row=2, column=0, sticky=W)
        self.load_bumpmap_settings(self.objectToModify.material.bmap) if self.objectToModify.material.bmap != None else self.bumpMapSettings()

        self.add_obj_btn = Button(self.add_obj_window, text='Apply', width=10, height=2, command=self.on_apply_obj_clicked)
        self.add_obj_btn.grid(row=5, column=0, sticky=W)
        self.close_btn = Button(self.add_obj_window, text='Back', width=10, height=2, command=self.on_back_clicked)
        self.close_btn.grid(row=5, column=1, sticky=E)

    def on_remove_object_clicked(self):

        objects = [obj for obj in self.render_engine.objects if not obj.isLight]
        index = int(self.objectsAdded.curselection()[0])
        obj = objects[index]
        index_to_remove = self.render_engine.objects.index(obj)
        oldObj = self.render_engine.objects[index_to_remove]
        self.render_engine.objects.remove(oldObj)
        self.updateObjectsAdded()


    def on_apply_obj_clicked(self):
        temp = [obj for obj in self.render_engine.objects if obj.id != self.objectToModify.id]
        self.render_engine.objects=temp
        self.on_add_obj_clicked()
        self.objectToModify = None
        self.updateObjectsAdded()

    def on_modify_light_clicked(self):

        self.add_obj_window = Toplevel(self.mainmenu)

        self.indexInGlobalList = int(self.lightsAdded.curselection()[0])
        self.objectToModify = self.render_engine.lights[self.indexInGlobalList]
        self.newObject = self.objectToModify

        self.objectSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Settings', width=50, height=50)
        self.objectSettingsLabelFrame.grid(row=0, column=1, columnspan=1, sticky=W)
        if isinstance(self.objectToModify, Triangle):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Triangle')
            self.load_triangle_settings(self.objectToModify)

        elif isinstance(self.objectToModify, Plane):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Plane')
            self.load_plane_settings(self.objectToModify)

        elif isinstance(self.objectToModify, Cube):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Cube')
            self.load_cube_settings(self.objectToModify)

        elif isinstance(self.objectToModify, Sphere):

            self.objectListBox = Listbox(self.add_obj_window, height=5, width=76)
            self.objectListBox.grid(row=0, column=0, columnspan=2, sticky=W)
            self.objectListBox.bind('<<ListboxSelect>>', self.on_select)
            self.objectListBox.insert(1, 'Sphere')
            self.load_sphere_settings(self.objectToModify)

        self.materialSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Material', width=50, height=50)
        self.materialSettingsLabelFrame.grid(row=1, column=1, columnspan=1, rowspan=2, sticky=W)
        self.load_material_settings(self.objectToModify.material)

        self.textureSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Texture', width=50, height=50)
        self.textureSettingsLabelFrame.grid(row=1, column=0, sticky=W)
        self.load_texture_settings(self.objectToModify.material.texture) if self.objectToModify.material.texture != None else self.textureSettings()

        self.bumpMapSettingsLabelFrame = LabelFrame(self.add_obj_window, text='Bump Map', width=50, height=50)
        self.bumpMapSettingsLabelFrame.grid(row=2, column=0, sticky=W)
        bumpmap = self.objectToModify.material.bmap if self.objectToModify.material.bmap != None else None
        self.load_bumpmap_settings(bumpmap) if bumpmap != None else self.bumpMapSettings()

        self.add_obj_btn = Button(self.add_obj_window, text='Apply', width=10, height=2, command=self.on_apply_light_clicked)
        self.add_obj_btn.grid(row=5, column=0, sticky=W)
        self.close_btn = Button(self.add_obj_window, text='Back', width=10, height=2, command=self.on_back_clicked)
        self.close_btn.grid(row=5, column=1, sticky=E)

    def on_remove_light_clicked(self):


        index = int(self.lightsAdded.curselection()[0])
        light = self.render_engine.lights[index]
        self.render_engine.lights.remove(light)
        self.updateLightsAdded()


    def on_apply_light_clicked(self):

        temp = [obj for obj in self.render_engine.lights if obj.id != self.objectToModify.id]
        self.render_engine.lights=temp
        self.on_add_obj_clicked()
        self.objectToModify = None
        self.updateLightsAdded()


    def updateObjectsAdded(self):

        self.objectsAdded.destroy()
        self.objectsAdded = Listbox(self.addedObjectsFrame, height=5, width=97)
        self.objectsAdded.grid(row=0, column=0)
        for i in xrange(len(self.render_engine.objects)):
            obj = self.render_engine.objects[i]
            if not obj.isLight:
                self.objectsAdded.insert(i, str(obj))



    def updateLightsAdded(self):

        self.lightsAdded.destroy()
        self.lightsAdded = Listbox(self.addedLightsFrame, height=5, width=97)
        self.lightsAdded.grid(row=0, column=0)
        for i in xrange(len(self.render_engine.lights)):
            obj = self.render_engine.lights[i]
            if obj.isLight:
                self.lightsAdded.insert(i, str(obj))




    def on_select(self, e):

        title = self.objectList[int(e.widget.curselection()[0])] if len(e.widget.curselection())>0 else self.objectList[0]
        self.objectSettingsLabelFrame.destroy()
        self.objectSettingsLabelFrame = LabelFrame(e.widget.master, text=title, width=150, height=50)
        self.objectSettingsLabelFrame.grid(row=0, column=1)
        if self.objectList.index(title) == 0:
            self.triangleSettings()

        elif self.objectList.index(title) == 1:

            self.planeSettings()

        elif self.objectList.index(title) == 2:

            self.cubeSettings()

        elif self.objectList.index(title) == 3:

            self.sphereSettings()



    #def on_select_added_element(self, e):



    def triangleSettings(self):

        self.classtype = 'Triangle'
        self.xV0Val = StringVar()
        self.yV0Val = StringVar()
        self.zV0Val = StringVar()

        self.xV1Val = StringVar()
        self.yV1Val = StringVar()
        self.zV1Val = StringVar()

        self.xV2Val = StringVar()
        self.yV2Val = StringVar()
        self.zV2Val = StringVar()


        self.xV0Val.set('0.0')
        self.yV0Val.set('0.0')
        self.zV0Val.set('0.0')

        self.xV1Val.set('0.0')
        self.yV1Val.set('0.0')
        self.zV1Val.set('0.0')

        self.xV2Val.set('0.0')
        self.yV2Val.set('0.0')
        self.zV2Val.set('0.0')

        self.labelV0 = Label(self.objectSettingsLabelFrame, text='V0 Point: <x, y, z>', width=35)
        self.labelV0.grid(row=0, column=0)
        self.xV0 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xV0Val)
        self.xV0.grid(row=0, column=1)
        self.yV0 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yV0Val)
        self.yV0.grid(row=0, column=2)
        self.zV0 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zV0Val)
        self.zV0.grid(row=0, column=3)

        self.labelV1 = Label(self.objectSettingsLabelFrame, text='V1 Point: <x, y, z>', width=35)
        self.labelV1.grid(row=1, column=0)
        self.xV1 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xV1Val)
        self.xV1.grid(row=1, column=1)
        self.yV1 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yV1Val)
        self.yV1.grid(row=1, column=2)
        self.zV1 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zV1Val)
        self.zV1.grid(row=1, column=3)

        self.labelV2 = Label(self.objectSettingsLabelFrame, text='V2 Point: <x, y, z>', width=35)
        self.labelV2.grid(row=2, column=0)
        self.xV2 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xV2Val)
        self.xV2.grid(row=2, column=1)
        self.yV2 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yV2Val)
        self.yV2.grid(row=2, column=2)
        self.zV2 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zV2Val)
        self.zV2.grid(row=2, column=3)

        self.isLight = BooleanVar()
        self.isLight.set('False')
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=3, column=0)

    def load_triangle_settings(self, triangle):

        self.classtype = 'Triangle'
        self.xV0Val = StringVar()
        self.yV0Val = StringVar()
        self.zV0Val = StringVar()

        self.xV1Val = StringVar()
        self.yV1Val = StringVar()
        self.zV1Val = StringVar()

        self.xV2Val = StringVar()
        self.yV2Val = StringVar()
        self.zV2Val = StringVar()


        self.xV0Val.set(str(triangle.v0.x))
        self.yV0Val.set(str(triangle.v0.y))
        self.zV0Val.set(str(triangle.v0.z))

        self.xV1Val.set(str(triangle.v1.x))
        self.yV1Val.set(str(triangle.v1.y))
        self.zV1Val.set(str(triangle.v1.z))

        self.xV2Val.set(str(triangle.v2.x))
        self.yV2Val.set(str(triangle.v2.y))
        self.zV2Val.set(str(triangle.v2.z))

        self.labelV0 = Label(self.objectSettingsLabelFrame, text='V0 Point: <x, y, z>', width=35)
        self.labelV0.grid(row=0, column=0)
        self.xV0 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xV0Val)
        self.xV0.grid(row=0, column=1)
        self.yV0 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yV0Val)
        self.yV0.grid(row=0, column=2)
        self.zV0 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zV0Val)
        self.zV0.grid(row=0, column=3)

        self.labelV1 = Label(self.objectSettingsLabelFrame, text='V1 Point: <x, y, z>', width=35)
        self.labelV1.grid(row=1, column=0)
        self.xV1 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xV1Val)
        self.xV1.grid(row=1, column=1)
        self.yV1 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yV1Val)
        self.yV1.grid(row=1, column=2)
        self.zV1 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zV1Val)
        self.zV1.grid(row=1, column=3)

        self.labelV2 = Label(self.objectSettingsLabelFrame, text='V2 Point: <x, y, z>', width=35)
        self.labelV2.grid(row=2, column=0)
        self.xV2 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xV2Val)
        self.xV2.grid(row=2, column=1)
        self.yV2 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yV2Val)
        self.yV2.grid(row=2, column=2)
        self.zV2 = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zV2Val)
        self.zV2.grid(row=2, column=3)

        self.isLight = BooleanVar()
        self.isLight.set(str(triangle.isLight))
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=3, column=0)



    def planeSettings(self):

        self.classtype = 'Plane'

        self.xTLVal = StringVar()
        self.yTLVal = StringVar()
        self.zTLVal = StringVar()

        self.xBLVal = StringVar()
        self.yBLVal = StringVar()
        self.zBLVal = StringVar()

        self.xTRVal = StringVar()
        self.yTRVal = StringVar()
        self.zTRVal = StringVar()

        self.xBRVal = StringVar()
        self.yBRVal = StringVar()
        self.zBRVal = StringVar()


        self.xTLVal.set('0.0')
        self.yTLVal.set('0.0')
        self.zTLVal.set('0.0')

        self.xBLVal.set('0.0')
        self.yBLVal.set('0.0')
        self.zBLVal.set('0.0')

        self.xTRVal.set('0.0')
        self.yTRVal.set('0.0')
        self.zTRVal.set('0.0')

        self.xBRVal.set('0.0')
        self.yBRVal.set('0.0')
        self.zBRVal.set('0.0')

        self.labelTL = Label(self.objectSettingsLabelFrame, text='Top Left Point: <x, y, z>', width=35)
        self.labelTL.grid(row=0, column=0)
        self.xTL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xTLVal)
        self.xTL.grid(row=0, column=1)
        self.yTL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yTLVal)
        self.yTL.grid(row=0, column=2)
        self.zTL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zTLVal)
        self.zTL.grid(row=0, column=3)

        self.labelBL = Label(self.objectSettingsLabelFrame, text='Bottom Left Point: <x, y, z>', width=35)
        self.labelBL.grid(row=1, column=0)
        self.xBL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xBLVal)
        self.xBL.grid(row=1, column=1)
        self.yBL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yBLVal)
        self.yBL.grid(row=1, column=2)
        self.zBL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zBLVal)
        self.zBL.grid(row=1, column=3)

        self.labelTR = Label(self.objectSettingsLabelFrame, text='Top Right Point: <x, y, z>', width=35)
        self.labelTR.grid(row=2, column=0)
        self.xTR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xTRVal)
        self.xTR.grid(row=2, column=1)
        self.yTR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yTRVal)
        self.yTR.grid(row=2, column=2)
        self.zTR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zTRVal)
        self.zTR.grid(row=2, column=3)

        self.labelBR = Label(self.objectSettingsLabelFrame, text='Bottom Right Point: <x, y, z>', width=35)
        self.labelBR.grid(row=3, column=0)
        self.xBR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xBRVal)
        self.xBR.grid(row=3, column=1)
        self.yBR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yBRVal)
        self.yBR.grid(row=3, column=2)
        self.zBR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zBRVal)
        self.zBR.grid(row=3, column=3)

        self.isLight = BooleanVar()
        self.isLight.set('False')
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=4, column=0, columnspan=1)

        self.reverse_normal = BooleanVar()
        self.reverse_normal.set('False')
        self.reverse_normal_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="Reverse Normal", variable=self.reverse_normal, onvalue=True, offvalue=False, width=35, anchor=W)
        self.reverse_normal_chkbox.grid(row=5, column=0, columnspan=1)

    def load_plane_settings(self, plane):

        self.classtype = 'Plane'

        self.xTLVal = StringVar()
        self.yTLVal = StringVar()
        self.zTLVal = StringVar()

        self.xBLVal = StringVar()
        self.yBLVal = StringVar()
        self.zBLVal = StringVar()

        self.xTRVal = StringVar()
        self.yTRVal = StringVar()
        self.zTRVal = StringVar()

        self.xBRVal = StringVar()
        self.yBRVal = StringVar()
        self.zBRVal = StringVar()


        self.xTLVal.set(str(plane.tl.x))
        self.yTLVal.set(str(plane.tl.y))
        self.zTLVal.set(str(plane.tl.z))

        self.xBLVal.set(str(plane.bl.x))
        self.yBLVal.set(str(plane.bl.y))
        self.zBLVal.set(str(plane.bl.z))

        self.xTRVal.set(str(plane.tr.x))
        self.yTRVal.set(str(plane.tr.y))
        self.zTRVal.set(str(plane.tr.z))

        self.xBRVal.set(str(plane.br.x))
        self.yBRVal.set(str(plane.br.y))
        self.zBRVal.set(str(plane.br.z))

        self.labelTL = Label(self.objectSettingsLabelFrame, text='Top Left Point: <x, y, z>', width=35)
        self.labelTL.grid(row=0, column=0)
        self.xTL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xTLVal)
        self.xTL.grid(row=0, column=1)
        self.yTL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yTLVal)
        self.yTL.grid(row=0, column=2)
        self.zTL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zTLVal)
        self.zTL.grid(row=0, column=3)

        self.labelBL = Label(self.objectSettingsLabelFrame, text='Bottom Left Point: <x, y, z>', width=35)
        self.labelBL.grid(row=1, column=0)
        self.xBL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xBLVal)
        self.xBL.grid(row=1, column=1)
        self.yBL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yBLVal)
        self.yBL.grid(row=1, column=2)
        self.zBL = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zBLVal)
        self.zBL.grid(row=1, column=3)

        self.labelTR = Label(self.objectSettingsLabelFrame, text='Top Right Point: <x, y, z>', width=35)
        self.labelTR.grid(row=2, column=0)
        self.xTR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xTRVal)
        self.xTR.grid(row=2, column=1)
        self.yTR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yTRVal)
        self.yTR.grid(row=2, column=2)
        self.zTR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zTRVal)
        self.zTR.grid(row=2, column=3)

        self.labelBR = Label(self.objectSettingsLabelFrame, text='Bottom Right Point: <x, y, z>', width=35)
        self.labelBR.grid(row=3, column=0)
        self.xBR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xBRVal)
        self.xBR.grid(row=3, column=1)
        self.yBR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yBRVal)
        self.yBR.grid(row=3, column=2)
        self.zBR = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zBRVal)
        self.zBR.grid(row=3, column=3)

        self.isLight = BooleanVar()
        self.isLight.set(str(plane.isLight))
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=4, column=0, columnspan=1)

        self.reverse_normal = BooleanVar()
        self.reverse_normal.set(str(plane.down))
        self.reverse_normal_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="Reverse Normal", variable=self.reverse_normal, onvalue=True, offvalue=False, width=35, anchor=W)
        self.reverse_normal_chkbox.grid(row=5, column=0, columnspan=1)

    def cubeSettings(self):

        self.classtype = 'Cube'

        self.labelOffset = Label(self.objectSettingsLabelFrame, text='Offset from origin: <dx, dy, dz>', width=35)
        self.labelOffset.grid(row=0, column=0)
        self.dx = StringVar()
        self.dy = StringVar()
        self.dz = StringVar()
        self.dx.set('0.0')
        self.dy.set('0.0')
        self.dz.set('0.0')
        self.xOff = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.dx)
        self.xOff.grid(row=0, column=1)
        self.yOff = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.dy)
        self.yOff.grid(row=0, column=2)
        self.zOff = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.dz)
        self.zOff.grid(row=0, column=3)

        self.labelScaling = Label(self.objectSettingsLabelFrame, text='Scaling per dimension: <sx, sy, sz>', width=35)
        self.labelScaling.grid(row=1, column=0)
        self.sx = StringVar()
        self.sy = StringVar()
        self.sz = StringVar()
        self.sx.set('1.0')
        self.sy.set('1.0')
        self.sz.set('1.0')
        self.xScal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.sx)
        self.xScal.grid(row=1, column=1)
        self.yScal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.sy)
        self.yScal.grid(row=1, column=2)
        self.zScal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.sz)
        self.zScal.grid(row=1, column=3)

        self.isLight = BooleanVar()
        self.isLight.set('False')
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=2, column=0)

    def load_cube_settings(self, cube):

        self.classtype = 'Cube'

        self.labelOffset = Label(self.objectSettingsLabelFrame, text='Offset from origin: <dx, dy, dz>', width=35)
        self.labelOffset.grid(row=0, column=0)
        self.dx = StringVar()
        self.dy = StringVar()
        self.dz = StringVar()
        self.dx.set(str(cube.dx))
        self.dy.set(str(cube.dy))
        self.dz.set(str(cube.dz))
        self.xOff = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.dx)
        self.xOff.grid(row=0, column=1)
        self.yOff = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.dy)
        self.yOff.grid(row=0, column=2)
        self.zOff = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.dz)
        self.zOff.grid(row=0, column=3)

        self.labelScaling = Label(self.objectSettingsLabelFrame, text='Scaling per dimension: <sx, sy, sz>', width=35)
        self.labelScaling.grid(row=1, column=0)
        self.sx = StringVar()
        self.sy = StringVar()
        self.sz = StringVar()
        self.sx.set(str(cube.sx))
        self.sy.set(str(cube.sy))
        self.sz.set(str(cube.sz))
        self.xScal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.sx)
        self.xScal.grid(row=1, column=1)
        self.yScal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.sy)
        self.yScal.grid(row=1, column=2)
        self.zScal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.sz)
        self.zScal.grid(row=1, column=3)

        self.isLight = BooleanVar()
        self.isLight.set(str(cube.isLight))
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=2, column=0)

    def sphereSettings(self):

        self.classtype = 'Sphere'

        self.labelCenter = Label(self.objectSettingsLabelFrame, text='Center Point: <x, y, z>', width=35)
        self.labelCenter.grid(row=0, column=0)
        self.xc = StringVar()
        self.yc = StringVar()
        self.zc = StringVar()
        self.xc.set('4.0')
        self.yc.set('4.0')
        self.zc.set('4.0')
        self.radius = StringVar()
        self.radius.set('2.0')
        self.xC = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xc)
        self.xC.grid(row=0, column=1)
        self.yC = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yc)
        self.yC.grid(row=0, column=2)
        self.zC = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zc)
        self.zC.grid(row=0, column=3)

        self.labelRadius = Label(self.objectSettingsLabelFrame, text='Radius :', width=35)
        self.labelRadius.grid(row=1, column=0)
        self.radiusVal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.radius)
        self.radiusVal.grid(row=1, column=1)

        self.isLight = BooleanVar()
        self.isLight.set('False')
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=2, column=0)

    def load_sphere_settings(self, sphere):

        self.classtype = 'Sphere'

        self.labelCenter = Label(self.objectSettingsLabelFrame, text='Center Point: <x, y, z>', width=35)
        self.labelCenter.grid(row=0, column=0)
        self.xc = StringVar()
        self.yc = StringVar()
        self.zc = StringVar()
        self.xc.set(str(sphere.c.x))
        self.yc.set(str(sphere.c.y))
        self.zc.set(str(sphere.c.z))
        self.radius = StringVar()
        self.radius.set(str(sphere.r))
        self.xC = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.xc)
        self.xC.grid(row=0, column=1)
        self.yC = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.yc)
        self.yC.grid(row=0, column=2)
        self.zC = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.zc)
        self.zC.grid(row=0, column=3)

        self.labelRadius = Label(self.objectSettingsLabelFrame, text='Radius :', width=35)
        self.labelRadius.grid(row=1, column=0)
        self.radiusVal = Entry(self.objectSettingsLabelFrame, width=5, textvariable=self.radius)
        self.radiusVal.grid(row=1, column=1)

        self.isLight = BooleanVar()
        self.isLight.set(bool(sphere.isLight))
        self.isLight_chkbox = Checkbutton(self.objectSettingsLabelFrame, text="is Light", variable=self.isLight, onvalue=True, offvalue=False, width=35, anchor=W)
        self.isLight_chkbox.grid(row=2, column=0)


    def materialSettings(self):


        self.colorLabel = Label(self.materialSettingsLabelFrame, text="Color <r,g,b> :", width=18)
        self.colorLabel.grid(row=0, column=0)

        self.r = StringVar()
        self.g = StringVar()
        self.b = StringVar()
        self.r.set('0.0')
        self.g.set('0.0')
        self.b.set('0.0')
        self.rValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.r)
        self.rValue.grid(row=0, column=1)
        self.gValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.g)
        self.gValue.grid(row=0, column=2)
        self.bValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.b)
        self.bValue.grid(row=0, column=3)
        self.selectColorBtn = Button(self.materialSettingsLabelFrame, text='Select color', command=self.on_select_color_click)
        self.selectColorBtn.grid(row=0, column=4)

        self.ka = StringVar()
        self.ke = StringVar()
        self.kd = StringVar()
        self.ks = StringVar()
        self.sh = StringVar()
        self.kt = StringVar()
        self.kgt = StringVar()
        self.kgts = StringVar()
        self.kr = StringVar()
        self.kgr = StringVar()
        self.kgrs = StringVar()
        self.ior = StringVar()


        self.ka.set('0.1')
        self.ke.set('0.0')
        self.kd.set('0.5')
        self.ks.set('0.9')
        self.sh.set('5.0')
        self.kt.set('0.0')
        self.kgt.set('0.0')
        self.kgts.set('0.0')
        self.kr.set('0.0')
        self.kgr.set('0.0')
        self.kgrs.set('0.0')
        self.ior.set('1.0')

        self.kaLabel = Label(self.materialSettingsLabelFrame, text="ka")
        self.kaLabel.grid(row=1, column=0)
        self.kaValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ka)
        self.kaValue.grid(row=1, column=1)

        self.keLabel = Label(self.materialSettingsLabelFrame, text="ke")
        self.keLabel.grid(row=2, column=0)
        self.keValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ke)
        self.keValue.grid(row=2, column=1)

        self.kdLabel = Label(self.materialSettingsLabelFrame, text="kd")
        self.kdLabel.grid(row=3, column=0)
        self.kdValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kd)
        self.kdValue.grid(row=3, column=1)

        self.ksLabel = Label(self.materialSettingsLabelFrame, text="ks")
        self.ksLabel.grid(row=4, column=0)
        self.ksValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ks)
        self.ksValue.grid(row=4, column=1)

        self.shLabel = Label(self.materialSettingsLabelFrame, text="sh")
        self.shLabel.grid(row=4, column=2)
        self.shValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.sh)
        self.shValue.grid(row=4, column=3)

        self.ktLabel = Label(self.materialSettingsLabelFrame, text="kt")
        self.ktLabel.grid(row=5, column=0)
        self.ktValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kt)
        self.ktValue.grid(row=5, column=1)

        self.iorLabel = Label(self.materialSettingsLabelFrame, text="Index of Refraction")
        self.iorLabel.grid(row=5, column=2)
        self.iorValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ior)
        self.iorValue.grid(row=5, column=3)

        self.kgtLabel = Label(self.materialSettingsLabelFrame, text="ktr")
        self.kgtLabel.grid(row=6, column=0)
        self.kgtValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgt)
        self.kgtValue.grid(row=6, column=1)

        self.kgtsSamplesLabel = Label(self.materialSettingsLabelFrame, text="No. Samples")
        self.kgtsSamplesLabel.grid(row=6, column=2)
        self.kgtsSamplesValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgts)
        self.kgtsSamplesValue.grid(row=6, column=3)

        self.krLabel = Label(self.materialSettingsLabelFrame, text="kr")
        self.krLabel.grid(row=7, column=0)
        self.krValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kr)
        self.krValue.grid(row=7, column=1)

        self.kgrLabel = Label(self.materialSettingsLabelFrame, text="kgr")
        self.kgrLabel.grid(row=8, column=0)
        self.kgrValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgr)
        self.kgrValue.grid(row=8, column=1)

        self.kgrSamplesLabel = Label(self.materialSettingsLabelFrame, text="No. Samples")
        self.kgrSamplesLabel.grid(row=8, column=2)
        self.kgrSamplesValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgrs)
        self.kgrSamplesValue.grid(row=8, column=3)

    def load_material_settings(self, material):
        self.materialColor = material.color
        self.colorLabel = Label(self.materialSettingsLabelFrame, text="Color <r,g,b> :", width=18)
        self.colorLabel.grid(row=0, column=0)

        self.r = StringVar()
        self.g = StringVar()
        self.b = StringVar()
        self.r.set(str(material.color[0]))
        self.g.set(str(material.color[1]))
        self.b.set(str(material.color[2]))
        self.rValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.r)
        self.rValue.grid(row=0, column=1)
        self.gValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.g)
        self.gValue.grid(row=0, column=2)
        self.bValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.b)
        self.bValue.grid(row=0, column=3)
        self.selectColorBtn = Button(self.materialSettingsLabelFrame, text='Select color', command=self.on_select_color_click)
        self.selectColorBtn.grid(row=0, column=4)

        self.ka = StringVar()
        self.ke = StringVar()
        self.kd = StringVar()
        self.ks = StringVar()
        self.sh = StringVar()
        self.kt = StringVar()
        self.kgt = StringVar()
        self.kgts = StringVar()
        self.kr = StringVar()
        self.kgr = StringVar()
        self.kgrs = StringVar()
        self.ior = StringVar()

        self.ka.set(str(material.ka))
        self.ke.set(str(material.ke))
        self.kd.set(str(material.kd))
        self.ks.set(str(material.ks))
        self.sh.set(str(material.sh))
        self.kt.set(str(material.kt))
        self.kgt.set(str(material.kgt))
        self.kgts.set(str(material.kgts))
        self.kr.set(str(material.kr))
        self.kgr.set(str(material.kgr))
        self.kgrs.set(str(material.kgrs))
        self.ior.set(str(material.ior))

        self.kaLabel = Label(self.materialSettingsLabelFrame, text="ka")
        self.kaLabel.grid(row=1, column=0)
        self.kaValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ka)
        self.kaValue.grid(row=1, column=1)

        self.keLabel = Label(self.materialSettingsLabelFrame, text="ke")
        self.keLabel.grid(row=2, column=0)
        self.keValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ke)
        self.keValue.grid(row=2, column=1)

        self.kdLabel = Label(self.materialSettingsLabelFrame, text="kd")
        self.kdLabel.grid(row=3, column=0)
        self.kdValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kd)
        self.kdValue.grid(row=3, column=1)

        self.ksLabel = Label(self.materialSettingsLabelFrame, text="ks")
        self.ksLabel.grid(row=4, column=0)
        self.ksValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ks)
        self.ksValue.grid(row=4, column=1)

        self.shLabel = Label(self.materialSettingsLabelFrame, text="sh")
        self.shLabel.grid(row=4, column=2)
        self.shValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.sh)
        self.shValue.grid(row=4, column=3)

        self.ktLabel = Label(self.materialSettingsLabelFrame, text="kt")
        self.ktLabel.grid(row=5, column=0)
        self.ktValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kt)
        self.ktValue.grid(row=5, column=1)

        self.iorLabel = Label(self.materialSettingsLabelFrame, text="Index of Refraction")
        self.iorLabel.grid(row=5, column=2)
        self.iorValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.ior)
        self.iorValue.grid(row=5, column=3)

        self.kgtLabel = Label(self.materialSettingsLabelFrame, text="kgt")
        self.kgtLabel.grid(row=6, column=0)
        self.kgtValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgt)
        self.kgtValue.grid(row=6, column=1)

        self.kgtsSamplesLabel = Label(self.materialSettingsLabelFrame, text="No. Samples")
        self.kgtsSamplesLabel.grid(row=6, column=2)
        self.kgtsSamplesValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgts)
        self.kgtsSamplesValue.grid(row=6, column=3)

        self.krLabel = Label(self.materialSettingsLabelFrame, text="kr")
        self.krLabel.grid(row=7, column=0)
        self.krValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kr)
        self.krValue.grid(row=7, column=1)

        self.kgrLabel = Label(self.materialSettingsLabelFrame, text="kgr")
        self.kgrLabel.grid(row=8, column=0)
        self.kgrValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgr)
        self.kgrValue.grid(row=8, column=1)

        self.kgrSamplesLabel = Label(self.materialSettingsLabelFrame, text="No. Samples")
        self.kgrSamplesLabel.grid(row=8, column=2)
        self.kgrSamplesValue = Entry(self.materialSettingsLabelFrame, width=5, textvariable=self.kgrs)
        self.kgrSamplesValue.grid(row=8, column=3)

    def textureSettings(self):

        self.textureLabel = Label(self.textureSettingsLabelFrame, text="Select an image to use as a Texture :", width=30)
        self.textureLabel.grid(row=0, column=0)


        self.texturePathLabel = Label(self.textureSettingsLabelFrame, text="None", width=50)
        self.texturePathLabel.grid(row=1, column=0, columnspan=4)

        self.selectTextureBtn = Button(self.textureSettingsLabelFrame, text='Browse...', command=self.on_select_texture_click)
        self.selectTextureBtn.grid(row=1, column=4, columnspan=1, sticky=E)

        self.removeTextureBtn = Button(self.textureSettingsLabelFrame, text='Remove', command=self.on_remove_texture_click)
        self.removeTextureBtn.grid(row=2, column=4)

        self.attenuation_factor = StringVar()
        self.attenuation_factor.set('0.0')
        self.attenuationFactorLabel = Label(self.textureSettingsLabelFrame, text="Color attenuation factor")
        self.attenuationFactorLabel.grid(row=3, column=0, sticky=W)
        self.attenuationFactorValue = Entry(self.textureSettingsLabelFrame, width=5, textvariable=self.attenuation_factor)
        self.attenuationFactorValue.grid(row=3, column=1, sticky=W)

        self.repeatforTexture = StringVar()
        self.repeatforTexture.set('1.0')
        self.repeatForLabel = Label(self.textureSettingsLabelFrame, text="Tiling value for dimension: ")
        self.repeatForLabel.grid(row=3, column=3, sticky=E)
        self.repeatForValue = Entry(self.textureSettingsLabelFrame, width=5, textvariable=self.repeatforTexture)
        self.repeatForValue.grid(row=3, column=4, sticky=E)



    def load_texture_settings(self, texture):
        self.textureFileName = texture.name
        self.textureLabel = Label(self.textureSettingsLabelFrame, text="Select an image to use as a Texture :", width=30)
        self.textureLabel.grid(row=0, column=0)

        text = 'None' if texture.name == None or texture == None else texture.name
        self.texturePathLabel = Label(self.textureSettingsLabelFrame, text=text, width=50)
        self.texturePathLabel.grid(row=1, column=0, columnspan=4)

        self.selectTextureBtn = Button(self.textureSettingsLabelFrame, text='Browse...', command=self.on_select_texture_click)
        self.selectTextureBtn.grid(row=1, column=4, columnspan=1, sticky=E)

        self.removeTextureBtn = Button(self.textureSettingsLabelFrame, text='Remove', command=self.on_remove_texture_click)
        self.removeTextureBtn.grid(row=2, column=4)

        self.attenuation_factor = StringVar()
        self.attenuation_factor.set(str(texture.attenutation))
        self.attenuationFactorLabel = Label(self.textureSettingsLabelFrame, text="Color attenuation factor")
        self.attenuationFactorLabel.grid(row=3, column=0, sticky=W)
        self.attenuationFactorValue = Entry(self.textureSettingsLabelFrame, width=5, textvariable=self.attenuation_factor)
        self.attenuationFactorValue.grid(row=3, column=1, sticky=W)

        self.repeatforTexture = StringVar()
        self.repeatforTexture.set(str(texture.repeatfor))
        self.repeatForLabel = Label(self.textureSettingsLabelFrame, text="Tiling value for dimension: ")
        self.repeatForLabel.grid(row=3, column=3, sticky=E)
        self.repeatForValue = Entry(self.textureSettingsLabelFrame, width=5, textvariable=self.repeatforTexture)
        self.repeatForValue.grid(row=3, column=4, sticky=E)

    def bumpMapSettings(self):

        self.bumpmapLabel = Label(self.bumpMapSettingsLabelFrame, text="Select an image to use as a Bump Map :")
        self.bumpmapLabel.grid(row=0, column=0)


        self.bumpmapPathLabel = Label(self.bumpMapSettingsLabelFrame, text='None', width=50)
        self.bumpmapPathLabel.grid(row=1, column=0, columnspan=4)

        self.selectBumpmapBtn = Button(self.bumpMapSettingsLabelFrame, text='Browse...', command=self.on_select_bumpmap_click)
        self.selectBumpmapBtn.grid(row=1, column=4, columnspan=1)

        self.removeBumpmapBtn = Button(self.bumpMapSettingsLabelFrame, text='Remove', command=self.on_remove_bumpmap_click)
        self.removeBumpmapBtn.grid(row=2, column=4)

        self.effect_factor = StringVar()
        self.effect_factor.set('0.0')
        self.effectFactorLabel = Label(self.bumpMapSettingsLabelFrame, text="Effect factor")
        self.effectFactorLabel.grid(row=3, column=0, sticky=W)
        self.effectFactorValue = Entry(self.bumpMapSettingsLabelFrame, width=5, textvariable=self.effect_factor)
        self.effectFactorValue.grid(row=3, column=1)

        self.repeatforBumpMap = StringVar()
        self.repeatforBumpMap.set('1.0')
        self.repeatForLabel = Label(self.bumpMapSettingsLabelFrame, text="Tiling value for dimension: ")
        self.repeatForLabel.grid(row=3, column=3)
        self.repeatForValue = Entry(self.bumpMapSettingsLabelFrame, width=5, textvariable=self.repeatforBumpMap)
        self.repeatForValue.grid(row=3, column=4)

    def load_bumpmap_settings(self, bumpmap):
        self.bumpMapFileName = bumpmap.name
        self.bumpmapLabel = Label(self.bumpMapSettingsLabelFrame, text="Select an image to use as a Bump Map :")
        self.bumpmapLabel.grid(row=0, column=0)

        text = 'None' if bumpmap.name == None else bumpmap.name
        self.bumpmapPathLabel = Label(self.bumpMapSettingsLabelFrame, text=text, width=50)
        self.bumpmapPathLabel.grid(row=1, column=0, columnspan=4)

        self.selectBumpmapBtn = Button(self.bumpMapSettingsLabelFrame, text='Browse...', command=self.on_select_bumpmap_click)
        self.selectBumpmapBtn.grid(row=1, column=4, columnspan=1)

        self.removeBumpmapBtn = Button(self.bumpMapSettingsLabelFrame, text='Remove', command=self.on_remove_bumpmap_click)
        self.removeBumpmapBtn.grid(row=2, column=4)

        self.effect_factor = StringVar()
        self.effect_factor.set(str(bumpmap.factor))
        self.effectFactorLabel = Label(self.bumpMapSettingsLabelFrame, text="Effect factor")
        self.effectFactorLabel.grid(row=3, column=0, sticky=W)
        self.effectFactorValue = Entry(self.bumpMapSettingsLabelFrame, width=5, textvariable=self.effect_factor)
        self.effectFactorValue.grid(row=3, column=1)

        self.repeatforBumpMap = StringVar()
        self.repeatforBumpMap.set(str(bumpmap.repeatfor))
        self.repeatForLabel = Label(self.bumpMapSettingsLabelFrame, text="Tiling value for dimension: ")
        self.repeatForLabel.grid(row=3, column=3)
        self.repeatForValue = Entry(self.bumpMapSettingsLabelFrame, width=5, textvariable=self.repeatforBumpMap)
        self.repeatForValue.grid(row=3, column=4)




    def on_select_color_click(self):

        color = askcolor(initialcolor=tuple([255, 0, 0])) if self.objectToModify == None else askcolor(initialcolor=tuple(self.objectToModify.material.color))
        if color[0] != None:
            self.materialColor = [color[0][0], color[0][1], color[0][2]]#  else self.materialColor
            self.r.set(str(self.materialColor[0]))
            self.g.set(str(self.materialColor[1]))
            self.b.set(str(self.materialColor[2]))


    def on_select_ambient_color_click(self):

        color = askcolor(initialcolor=tuple(self.ambientColor))
        self.ambientColor = [color[0][0], color[0][1], color[0][2]] if color[0] != None else self.ambientColor
        self.rAmbient.set(str(self.ambientColor[0]))
        self.gAmbient.set(str(self.ambientColor[1]))
        self.bAmbient.set(str(self.ambientColor[2]))
        self.render_engine.createAmbientLight(self.ambientColor)


    def on_select_texture_click(self):

        self.textureFileName = askopenfilename(initialdir='textures')
        self.textureFileName = str(self.textureFileName)
        currentDir = os.getcwd().replace('\\', '/')
        self.textureFileName=self.textureFileName.replace(currentDir, '.')
        self.texturePathLabel['text'] = self.textureFileName if self.textureFileName != '' else "None"

    def on_select_bumpmap_click(self):

        self.bumpMapFileName = askopenfilename(initialdir='bump_maps')
        self.bumpMapFileName = str(self.bumpMapFileName)
        currentDir = os.getcwd().replace('\\', '/')
        self.bumpMapFileName=self.bumpMapFileName.replace(currentDir, '.')
        self.bumpmapPathLabel['text'] = self.bumpMapFileName if self.bumpMapFileName != '' else "None"

    def on_select_environment_click(self):

        self.envFileName = askopenfilename(initialdir='skyboxes')
        self.envFileName = str(self.envFileName)
        currentDir = os.getcwd().replace('\\', '/')
        self.envFileName=self.envFileName.replace(currentDir, '.')
        self.envPathLabel['text'] = self.envFileName if self.envFileName != '' else "None"
        self.render_engine.create_environment_map(self.envFileName) if self.envFileName != None and self.envFileName != '' and self.envFileName != 'None' else "None"

    def on_remove_environment_click(self):

        self.envFileName = 'None'
        self.envPathLabel['text'] = self.envFileName if self.envFileName != None and self.envFileName != '' and self.envFileName != 'None' else "None"
        self.render_engine.create_environment_map(self.envFileName) if self.envFileName != None else None# and self.envFileName != '' and self.envFileName != 'None' else "None"

    def on_remove_texture_click(self):

        self.textureFileName = None
        self.texturePathLabel['text'] = 'None'#self.textureFileName if self.textureFileName != None else 'None'# and self.textureFileName != '' and self.textureFileName != 'None' else "None"
        #self.render_engine.create_environment_map(self.envFileName) if self.envFileName != None else None# and self.envFileName != '' and self.envFileName != 'None' else "None"

    def on_remove_bumpmap_click(self):

        self.bumpMapFileName = None
        self.bumpmapPathLabel['text'] = 'None'#self.bumpMapFileName if self.bumpMapFileName != None else 'None'#and self.bumpMapFileName != '' and self.bumpMapFileName != 'None' else "None"
        #self.render_engine.create_environment_map(self.envFileName) if self.envFileName != None else None# and self.envFileName != '' and self.envFileName != 'None' else "None"






