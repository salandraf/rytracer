__author__ = 'Francesco'

from utils.Constants import *
from numpy import inf

class Ray:

    def __init__(self, o, dir):

        """
        This class implements a Ray object. The parameters used are the following:
        :param o: a Point instance, is the origin point for the current ray
        :param dir: a Vector instance, is the direction of the current ray
        :return: a Ray object instance
        """
        self.o = o
        self.d = dir

        '''In the initialization phase the following additional parameters are used:
        :param i: an istance of Point, represents the current intersection point for the ray.
                  Initially the intersection point is set at infinite distance
        :param iobj: an instance of GeometricObject interface, represents the object corresponding to the minimum
                     intersection point for the ray. Initially is set to None.
        :param type: a string, represents an identifier for the current ray.
        :param color: a List object containing the r, g, b values for the intersection point.
                     Initially color is black.
        :param intersections: a List object, containing instances of Point, representing all the intersection points
                              for the current ray with any object in the scene.'''
        self.i = inf_point
        self.iobj = None
        self.type = 'primary'
        self.color = [0.0, 0.0, 0.0]
        self.intersections = []


    def __str__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Ray := { origin := '+str(self.o)+', direction := '+str(self.d)+'}'


    def intersect(self, scene):

        """
        This function calls the corresponding intersect() function for each object in the scene.
        The intersect() function returns the complete list of coefficients (alfa) used to compute the real intersection
        point. The closest intersection point is then extracted from the list and using his index in the list, also
        the corresponding object and the color are extracted.
        :param scene:
        :return:
        """

        alfas = []
        distances = []
        colors = []
        hits = []
        faces = []
        o = self.o
        d = self.d


        for obj in scene:
            alfa = obj.intersect(self)
            ip = o + d * alfa if alfa != inf else inf_point
            dist = (ip - o).dot(ip-o)
            alfas.append(alfa)
            distances.append(dist)
            hits.append(obj)
            colors.append(obj.material.color)

        '''
        if the list of alfas is void, then there is no intersection point for the current ray. The list is filled
        with the infinite point only to avoid errors during the computation.
        '''
        if len(alfas) == 0:
            alfas = [np.inf]
        else:
            '''
            if the list of alfas is not void, then the minimum alfa is extracted in order to get the closest
            intersection point for the current ray
            '''
            min_alfa = min(alfas)
            index = alfas.index(min_alfa)
            i = o + d * min_alfa if min_alfa != inf else inf_point

            '''updating attributes due to presence of an intersection point'''
            self.i = i
            self.color = colors[index] if min_alfa != inf else self.color
            self.iobj = hits[index]

            '''color is computed on the basis of the presence of a Texture object'''
            if (self.iobj.material.texture != None and min_alfa != inf) and i != inf_point:
                texture_color = self.iobj.read_texture(i)
                self.color = [texture_color[j]*self.iobj.material.texture.attenutation for j in range(len(self.color))]










