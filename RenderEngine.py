__author__ = 'Francesco'

from cv2 import imread, split, merge

from pylab import imshow, pause, ion, show, imsave, axis
from numpy import zeros, float32, clip, sqrt, random, linspace
from random import uniform
from basic_elements.Point import Point
from basic_elements.Vector import Vector
from Camera import Camera
from Texture import Texture
from BumpMap import BumpMap
from Ray import Ray
from materials.Material import Material
from primitives.Sphere import Sphere
from primitives.Cube import Cube
from primitives.Plane import Plane
from primitives.Triangle import Triangle
from lights.Ambient import Ambient
from Shader import Shader
from Viewplane import Viewplane
from Tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure


class RenderEngine:

    def __init__(self):
        '''This is the renderer class'''
        self.objects = []
        self.lights = []
        self.camera = None
        self.ambient = None
        self.skybox = None
        self.antialiasing_samples = 0
        self.jittering_samples = 0
        self.dof_samples = 0
        self.sshadows_samples = 0
        self.level_of_recursion = 0
        self.samples_ktr = 0
        self.samples_kgr = 0

    def add_object(self, classtype, dict):

        if classtype == 'Triangle':

            V0 = dict['v0']
            V1 = dict['v1']
            V2 = dict['v2']

            v0 = Point(V0[0], V0[1], V0[2])
            v1 = Point(V1[0], V1[1], V1[2])
            v2 = Point(V2[0], V2[1], V2[2])
            material = dict['material']
            isLight = bool(dict['isLight'])
            new_triangle = Triangle(v0, v1, v2, material)
            new_triangle.isLight=bool(isLight)
            self.objects.append(new_triangle) if not isLight else self.lights.append(new_triangle)


        elif classtype == 'Plane':

            TL = dict['tl']
            BL = dict['bl']
            TR = dict['tr']
            BR = dict['br']
            reverse_normal = dict['reverse_normal']
            material = dict['material']
            isLight = bool(dict['isLight'])

            tl = Point(TL[0], TL[1], TL[2])
            bl = Point(BL[0], BL[1], BL[2])
            tr = Point(TR[0], TR[1], TR[2])
            br = Point(BR[0], BR[1], BR[2])
            new_plane = Plane(tl, bl, tr, br, material, reverse_normal)
            new_plane.isLight=bool(isLight)
            self.objects.append(new_plane) if not isLight else self.lights.append(new_plane)

        elif classtype == 'Cube':

            material = dict['material']
            isLight = bool(dict['isLight'])
            dx = dict['dx']
            dy = dict['dy']
            dz = dict['dz']
            sx = dict['sx']
            sy = dict['sy']
            sz = dict['sz']

            new_cube = Cube(material, isLight, dx, dy, dz, sx, sy, sz)
            self.objects.append(new_cube) if not isLight else self.lights.append(new_cube)

        elif classtype == 'Sphere':

            center = dict['center']
            center = Point(center[0], center[1], center[2])
            radius = dict['radius']
            material = dict['material']
            isLight = bool(dict['isLight'])

            new_sphere = Sphere(center, radius, material)
            new_sphere.isLight =bool(isLight)
            self.objects.append(new_sphere) if not isLight else self.lights.append(new_sphere)

    def setupCamera(self, pov, lookat, up, focal_length, f_number):

        pov = Point(pov[0], pov[1], pov[2])
        lookat = Point(lookat[0], lookat[1], lookat[2])
        up = Vector(up[0], up[1], up[2])
        camera = Camera(pov, lookat, up, focal_length, f_number)
        self.camera = camera

    def create_material(self, dict):

        ka = dict['ka']
        ke = dict['ke']
        kd = dict['kd']
        ks = dict['ks']
        sh = dict['sh']
        kr = dict['kr']
        kgr = dict['kgr']
        kgrs = dict['kgrs']
        kt = dict['kt']
        kgt = dict['kgt']
        kgts = dict['kgts']
        ior = dict['ior']
        color = dict['color']
        texture = dict['texture']
        bumpmap = dict['bumpmap']
        material = Material(ka, ke, kd, ks, sh, kt, kgt, kgts, kr, kgr, kgrs,ior, color, texture, bumpmap)
        return material

    def createAmbientLight(self, color):

        self.ambient = Ambient(color)

    def setupViewPlane(self, hres, aspect):

        vres = round(hres / aspect)
        distance = vres/2
        self.viewplane = Viewplane(hres, vres, distance)



    def create_texture(self, dict):

        name = dict['name']
        attenuation = dict['attenuation']
        repeatfor = dict['repeatfor']

        texture = Texture(name, attenuation, repeatfor) if name != None else None

        return texture

    def create_bump_map(self, dict):

        mapname = dict['name']
        effect_factor = dict['effect_factor']
        repeatfor = dict['repeatfor']

        bumpmap = BumpMap(mapname, effect_factor, repeatfor) if mapname != None else None
        return bumpmap

    def create_environment_map(self, path):

        self.skybox = path

    def start_rendering(self, antialiasing_on, soft_shadows_on, jittering_on, depth_of_field_on):

        scene = self.objects + self.lights
        ambient = self.ambient
        camera = self.camera
        viewplane = self.viewplane

        raw_scene = self.elaborate_scene(scene)
        img = zeros((viewplane.height, viewplane.width), dtype=float32)
        imsave('test.png', img)
        img = imread('test.png')
        b, g, r = split(img)
        ion()
        image = imshow(img, animated=True)
        axis('off')
        '''The jittering is the first feature with an impact on the way image is rendered'''
        if jittering_on:
            r, g, b = self.jitter(image, r, g, b, depth_of_field_on, soft_shadows_on)

        else:
            for v in xrange(int(viewplane.height)):
                for u in xrange(int(viewplane.width)):

                    '''Then we manage the antialiasing feature'''
                    if antialiasing_on:
                        r, g, b = self.antialiasing(r, g, b, soft_shadows_on)
                        img = merge((r, g, b))
                    else:
                        '''if we do not use antialiasing we must check for the use of depth of field and soft shadows'''
                        '''since soft shadows are triggered using a flag directly onto the shader we split the computation
                        flow by checking depth of field usage'''
                        if depth_of_field_on:
                            r,g,b = self.depth_of_field(r, g, b,soft_shadows_on)

                        else:
                            '''we are not using anything else apart for the soft shadows, eventually'''
                            px = camera.eye + camera.u * (u + 0.5 - viewplane.width*0.5) + camera.v * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
                            #dist = sqrt((px-camera.eye).dot(px-camera.eye))# - distance
                            #px = camera.eye + camera.u  * (u + 0.5 - hres*0.5) + camera.v  * ((vres-v) + 0.5 - vres*0.5) + camera.w * dist
                            d = (px - camera.eye).normalize()
                            ray = Ray(camera.eye, d)
                            ray.intersect(raw_scene)
                            shader = Shader(ray, scene, ambient, camera, self.level_of_recursion, soft_shadows_on, self.skybox)
                            shader.shade()
                            r[v][u] = clip(ray.color[0], 0, 255)
                            g[v][u] = clip(ray.color[1], 0, 255)
                            b[v][u] = clip(ray.color[2], 0, 255)

                        img = merge((r, g, b))
                image.set_data(img)
                pause(0.0000001)
        img = merge((r, g, b))
        show(block=True)
        imsave('test.png', img)


    def elaborate_scene(self, scene):
            raw_scene = []
            for obj in scene:
                if hasattr(obj, 'faces'):
                    faces=getattr(obj, 'faces')
                    #raw_scene.remove(obj)
                    for face in faces:
                        raw_scene.append(face)
                else:
                    raw_scene.append(obj)
            return raw_scene


    def depth_of_field(self, r, g, b, soft_shadows_on):

        scene = self.objects + self.lights
        ambient = self.ambient
        camera = self.camera
        viewplane = self.viewplane

        raw_scene = self.elaborate_scene(scene)
        for v in xrange(int(viewplane.height)):
            for u in xrange(int(viewplane.width)):
                samples = int(self.dof_samples)
                color = [0, 0, 0]
                f = camera.focal_length
                fnumber = camera.f_number
                aperture = f/fnumber

                '''This is needed to avoid unintended zooming caused by moving the focus plane'''
                coeff = f/float(viewplane.distance)

                px = camera.eye + camera.u *coeff * (u + 0.5 - viewplane.width*0.5) + camera.v *coeff * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
                dist = sqrt((px-camera.eye).dot(px-camera.eye))# - distance
                px = camera.eye + camera.u *coeff * (u + 0.5 - viewplane.width*0.5) + camera.v *coeff * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * dist
                d = (px - camera.eye).normalize()

                for i in xrange(samples):

                    half_aperture = aperture*0.5
                    uoffset = uniform(-half_aperture, half_aperture)
                    voffset = uniform(-half_aperture, half_aperture)
                    lensp = camera.eye + camera.u * uoffset + camera.v * voffset

                    fp = px + camera.w * -(f+dist)

                    d = (fp - lensp).normalize()
                    dof_ray = Ray(lensp, d)
                    dof_ray.intersect(raw_scene)
                    shader = Shader(dof_ray, scene, ambient, camera, self.level_of_recursion, soft_shadows_on, self.skybox)
                    shader.shade()

                    color = [color[k] + dof_ray.color[k] for k in xrange(len(color))]

                r[v][u] = clip((color[0]/samples), 0, 255)
                g[v][u] = clip((color[1]/samples), 0, 255)
                b[v][u] = clip((color[2]/samples), 0, 255)

        return r, g, b

    def jitter(self, image, r, g, b, depth_of_field_on, soft_shadows_on):

        scene = self.objects + self.lights
        ambient = self.ambient
        camera = self.camera
        viewplane = self.viewplane

        raw_scene = self.elaborate_scene(scene)
        points = viewplane.height * viewplane.width * 2
        for i in xrange(int(points)):

            u = random.sample() * viewplane.width
            v = random.sample() * viewplane.height

            if depth_of_field_on:
                r, g, b = self.depth_of_field(r, g, b, soft_shadows_on)


            else:
                px = camera.eye + camera.u * (u + 0.5 - viewplane.width*0.5) + camera.v * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
                d = (px - camera.eye).normalize()
                ray = Ray(camera.eye, d)
                ray.intersect(raw_scene)
                shader = Shader(ray, scene, ambient, camera, self.level_of_recursion, soft_shadows_on, self.skybox)
                shader.shade()
                r[v][u] = clip(ray.color[0], 0, 255)
                g[v][u] = clip(ray.color[1], 0, 255)
                b[v][u] = clip(ray.color[2], 0, 255)
            img = merge((r, g, b))
            image.set_data(img)
            pause(0.0000001)

        return r, g, b

    def antialiasing(self, r, g, b, soft_shadows_on):

        scene = self.objects + self.lights
        ambient = self.ambient
        camera = self.camera
        viewplane = self.viewplane

        raw_scene = self.elaborate_scene(scene)
        for v in xrange(int(viewplane.height)):
            for u in xrange(int(viewplane.width)):
                color = [0,0,0]
                px = camera.eye + camera.u * (u + 0.5 - viewplane.width*0.5) + camera.v * ((viewplane.height-v) + 0.5 - viewplane.height*0.5) + camera.w * -viewplane.distance
                sqrt_samples = sqrt(int(self.antialiasing_samples))
                range_max = 1/float(sqrt_samples)
                for i in xrange(int(self.antialiasing_samples)):
                    px = px + camera.u * random.uniform(0, range_max) + camera.v * random.uniform(0, range_max)
                    d = (px - camera.eye).normalize()
                    ray = Ray(camera.eye, d)
                    ray.intersect(raw_scene)
                    shader = Shader(ray, scene, ambient, camera, self.level_of_recursion, soft_shadows_on, self.skybox)
                    shader.shade()
                    color = [color[k] + 1/float(self.antialiasing_samples)*ray.color[k] for k in range(len(color))]
                r[v][u] = clip(color[0], 0, 255)
                g[v][u] = clip(color[1], 0, 255)
                b[v][u] = clip(color[2], 0, 255)

        return r, g, b






