__author__ = 'Francesco'

from cv2 import imread, split
from numpy import clip

class Texture:

    def __init__(self, name, attenuation, repeatfor):

        """
        This class models a Texture object. The parameters used are the following
        :param name: a string, the relative path of the image file.
        :param attenuation: a float, which indicates how much the intensity of the color has to be reduced for the texture
        :param repeatfor: a number, which indicates how many times the texture has to be repeated. The number
                            stands for n-times in height and n-times in width
        :return: a Texture object
        """

        self.name = name
        self.image = imread(name)
        self.attenutation = attenuation if (attenuation > 0 and attenuation <=1) \
                                        else 1

        self.repeatfor = int(repeatfor)
        self.sizeV = self.image.shape[0]
        self.sizeU = self.image.shape[1]
        self.aspect = self.sizeU/float(self.sizeV)
        b, g, r = split(self.image)
        self.r = r
        self.g = g
        self.b = b


    def read(self, u, v):

        """
        This function reads the texel on the image associated to the Texture object, provided the coordinates in the
        tangent space (u, v). Since these coordinates are in range [0,1], the dimensions of the image are used to
        get the right pixel (by using an interpolation)
        :param u: a float number between 0 and 1
        :param v: a float number between 0 and 1
        :return: a List object containing the r, g, b values for the color of the texel
        """

        '''vertical coordinate v has to be taken upside-down because of the way images are accessed'''
        x = abs(u)*self.sizeU
        y = (1-abs(v))*self.sizeV

        '''
        if the texture must be repeated, we have to start reading again from 0 when we overflow the max level for
        that dimension
        '''
        coeff = [i for i in range(1, self.repeatfor) if x > i*self.sizeU]
        coeff = coeff[len(coeff)-1] if len(coeff) > 0 \
                                    else 0

        tiled_x = abs(x) - coeff*self.sizeU
        x = tiled_x if tiled_x > 0 \
                    else x

        coeff = [i for i in range(0, self.repeatfor) if abs(y) > i*self.sizeV]
        coeff = coeff[len(coeff)-1] if len(coeff) > 0 \
                                    else 0

        tiled_y = abs(y) - coeff*self.sizeV
        y = tiled_y if y > 0 \
                    else (self.sizeV - tiled_y)

        x = clip([x], 0.0, self.sizeU-1)[0]
        y = clip([y], 0.0, self.sizeV-1)[0]

        '''
        we reduce the color intensity of the image by multiplying for the attenuation factor.
        This because textures use full range from 0 to 255 and if then illumination is applied from shading,
        the resulting colors will be all burned to white. It is necessary to find a balance between light strenght
        in the scene and the attenuation factor.
        '''
        color_r = self.r[y, x]*self.attenutation
        color_g = self.g[y, x]*self.attenutation
        color_b = self.b[y, x]*self.attenutation

        return [color_r, color_g, color_b]





