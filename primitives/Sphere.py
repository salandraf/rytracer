__author__ = 'Francesco'

from interfaces.GeometricObject import GeometricObject
from utils.Constants import *
from basic_elements.Point import Point
from basic_elements.Vector import Vector
from numpy import inf, pi, interp
from math import sqrt, atan2, acos
import random

class Sphere(GeometricObject):


    def __init__(self, center, radius, material):

        """
        This class implements the GeometricObject interface, inheriting all of its attributes
        and models a Sphere by specifying the following parameters:
        :param center: a Point instance
        :param radius: a number which specifies the radius of the sphere
        :param material: a Material object instance
        :return: a Sphere object instance
        """

        GeometricObject.__init__(self)

        self.c = center
        self.r = radius
        self.material = material


    def __eq__(self, other):

        """
        This functions determines if this instance of Sphere is equal to another instance of Sphere
        :param other: a Sphere instance
        :return: a boolean value.
        """

        return self.id == other.id if other != None else False


    def __ne__(self, other):

        return not self.__eq__(other)#self.c != other.c if hasattr(other, 'c') else False


    def __lt__(self, other):

        return self.c < other.c


    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Sphere := { center := '+str(self.c)+', radius := '+str(self.r)+', material := '+str(self.material)+'}'


    def normal(self, p):

        """
        This function calculates the normal to the surface based on a specific Point
        :param p: a Point instance
        :return: a Vector instance (the normal vector to the surface)
        """

        n = (p - self.c).normalize()
        return n


    def intersect(self, ray):

        """
        This function implements the ray-sphere intersection algorithm.
        :param ray: an instance of Ray
        :return: a float representing the alfa parameter to use in the equation of the ray-sphere intersection
        """

        v = (ray.o - self.c)
        a = ray.d.dot(ray.d)
        b = 2*ray.d.dot(v)
        c = v.dot(v) - self.r*self.r

        self.a = a

        delta = b*b - 4*a*c

        if delta >= 0:

            alfa1 = (-b - sqrt(delta))/(2*a)
            alfa2 = (-b + sqrt(delta))/(2*a)

            if alfa1 > kEpsilon and alfa2 > kEpsilon:
                '''outside, before the sphere'''
                alfa = min(alfa1, alfa2)
                return alfa

            elif alfa1 > kEpsilon and alfa2 < kEpsilon:
                '''inside the sphere'''
                alfa = alfa1
                return alfa

            elif alfa1 < kEpsilon and alfa2 > kEpsilon:
                '''outside, after the sphere'''
                alfa = alfa2
                return alfa

            else:

                return inf
        else:

            return inf


    def map_point_on_texture(self, p):

        """
        This function takes as input a point and calculates its coordinates in tangent space to perform texture mapping.
        In this case spherical coordinates are used.
        :param p: a Point instance
        :return: u,v float coordinates in tangent space
        """

        N = self.normal(p)
        u = (atan2(N.x, N.z))+2.5
        v = (acos(-N.y)/pi)
        u = u + 2*pi if u < 0 \
                     else u
        u /= 2*pi
        return u, v


    def aspect(self):

        """
        This function calculates the width of the sphere as its circumference
        and the height of the sphere using its diameter.
        :return: a List object containing width and height
        """

        w = self.r * 2 * pi
        h = self.r * 2

        return [w, h]


    def get_width(self):

        """
        This functions returns the width of the sphere by using its circumference.
        :return: a float number corresponding to the width of the sphere.
        """

        return 2*self.r


    def sample_obj(self, num_samples):

        """
        This functions performs Monte Carlo Sampling on the object when it is declared as a light object into
        the scene. This techniques is used when modeling soft shadows returning a list of points randomly taken
        to calculate the direction of shadow rays.
        :param num_samples: a number indicating the number of samples we want to use to perform sampling
        :return: a List object containing instances of Points
        """

        points = []
        dx = self.c.x + random.uniform(-self.r, self.r)
        dy = self.c.y + random.uniform(-self.r, self.r)
        dz = self.c.z + random.uniform(-self.r, self.r)

        '''For each sample we use the same random points on the sphere'''
        for s in xrange(num_samples):
            p = Point(dx, dy, dz)
            points.append(p)

        return points


    def perturbe_normal(self, p):

        """
        This function calculates a new normal vector to the object surface based on the normal map declared in the
        object's material. The new normal is calculated by using a perturbation vector which deviates the original
        direction of the normal.
        :param p: a Point instance
        :return: a Vector instance (the new normal vector)
        """

        '''
        we need to read the "color" of the normal map, which associates the [r, g, b] values to the [x, y, z]
        coordinates of the normal vector in that specific point
        '''
        u, v = self.map_point_on_texture(p)
        color = self.material.bmap.read(u, v)
        color[0] = interp(color[0], [0, 255], [-1, 1]) * self.material.bmap.factor
        color[1] = interp(color[1], [0, 255], [-1, 1]) * self.material.bmap.factor
        color[2] = interp(color[2], [0, 255], [-1, 1]) * self.material.bmap.factor

        N = self.normal(p)
        up = Vector(0.0,1.0,0.0)
        udir = up.cross_p(N).normalize()
        vdir = N.cross_p(udir).normalize()

        '''Calculation of the perturbation vector D'''
        D = udir * color[0] + vdir * color[1] + N*color[2]

        '''Normal vector perturbation'''
        N = N + D

        return N.normalize()


    def read_texture(self, p):

        """
        This function retrieve the color of the texture associated with the object (if any) by calculating
        the u,v coordinates in tangent space and by calling the read() function on the Texture object itself.
        :param p: a Point instance
        :return: a List object containing the r, g, b values
        """

        u, v = self.map_point_on_texture(p)
        color = self.material.texture.read(u, v)
        return color


