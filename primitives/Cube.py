__author__ = 'Francesco'

from primitives.Plane import *
from basic_elements.Matrix import *
from utils.Constants import *

class Cube(GeometricObject):

    def __init__(self, material, isLight, dx, dy, dz, sx, sy, sz):

        """
        This class implements the GeometricObject interface, inheriting all of its attributes
        and models a Cube by specifying the following parameters:
        :param material: a Material instance
        :param isLight: a boolean value which determines if the current object is a light in the scene
        :param dx: offset on the world's x axis
        :param dy: offset on the world's y axis
        :param dz: offset on the world's z axis
        :param sx: scaling factor on the world's x axis
        :param sy: scaling factor on the world's y axis
        :param sz: scaling factor on the world's z axis
        :return: a Cube object instance
        """

        GeometricObject.__init__(self)

        self.material = material

        self.dx = dx
        self.dy = dy
        self.dz = dz

        self.sx = sx
        self.sy = sy
        self.sz = sz

        '''the cube is build from a set of standard, axis-aligned, planes'''
        f = Plane(Point(0, 1, 1), Point(0, 0, 1), Point(1, 1, 1), Point(1, 0, 1), material, False)
        b = Plane(Point(1, 1, 0), Point(1, 0, 0), Point(0, 1, 0), Point(0, 0, 0), material, False)
        t = Plane(Point(0, 1, 0), Point(0, 1, 1), Point(1, 1, 0), Point(1, 1, 1), material, False)
        d = Plane(Point(0, 0, 0), Point(0, 0, 1), Point(1, 0, 0), Point(1, 0, 1), material, False)
        l = Plane(Point(0, 1, 0), Point(0, 0, 0), Point(0, 1, 1), Point(0, 0, 1), material, False)
        r = Plane(Point(1, 1, 1), Point(1, 0, 1), Point(1, 1, 0), Point(1, 0, 0),  material, False)

        '''the center of the cube has to be determined in order to be managed as a light'''
        self.c = Point(0.5, 0.5, 0.5)

        '''if the cube is a light object, then all of its planes must be declared as light objects'''
        self.isLight = isLight
        f.isLight = isLight
        b.isLight = isLight
        t.isLight = isLight
        d.isLight = isLight
        l.isLight = isLight
        r.isLight = isLight

        f.light_intensity = self.light_intensity
        b.light_intensity = self.light_intensity
        t.light_intensity = self.light_intensity
        d.light_intensity = self.light_intensity
        l.light_intensity = self.light_intensity
        r.light_intensity = self.light_intensity


        self.front = f
        self.back = b
        self.top = t
        self.down = d
        self.left = l
        self.right = r

        '''this attribute is important because if an object declares it, then it can be decomposed in the scene'''
        self.faces = [f, b, l, r, t, d]

        '''
        here the scaling and translations transformations are applied on the basis of the offsets and scaling
        factors declared (so that a cube can become also a parallelepiped
        '''
        self.scale(self.sx, self.sy, self.sz)
        self.translate(self.dx, self.dy, self.dz)


    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        string = 'Cube := { front := '+str(self.front)+', back := '+str(self.back)+', '
        string += 'top := '+str(self.top)+', down := '+str(self.down)+', '
        string += 'left := '+str(self.left)+', right := '+str(self.right)+', '
        string += 'material := '+str(self.front.material)+'}'
        return string


    def scale(self, sx, sy, sz):

        faces = [self.front, self.back, self.left, self.right, self.down, self.top]
        for face in faces:
            face.scale_points(sx, sy, sz)
        self.scale_point(self.c, sx, sy, sz)


    def translate(self, dx, dy, dz):

        """
        This functions calls the translation transformation on every plane of the cube
        by taking as input the following parameters:
        :param dx: offset on the world's x axis
        :param dy: offset on the world's y axis
        :param dz: offset on the world's z axis
        :return: void. The transformation takes place directly on the Point instances of the object.
        """

        faces = [self.front, self.back, self.left, self.right, self.down, self.top]
        for face in faces:
            face.translate_points(dx, dy, dz)

        '''we must translate also the center of the cube'''
        self.translate_point(self.c, dx, dy, dz)


    def translate_point(self, p, dx, dy, dz):

        """
        This function applies the translation transformation matrix on the provided point.
        :param p: the original Point instance
        :param dx: offset on the world's x axis
        :param dy: offset on the world's y axis
        :param dz: offset on the world's z axis
        :return: a Point instance
        """

        f1 = Vector(1, 0, 0, dx)
        f2 = Vector(0, 1, 0, dy)
        f3 = Vector(0, 0, 1, dz)
        f4 = Vector(0, 0, 0, 1)

        TR = Matrix(f1, f2, f3, f4)

        return TR.dot(p)


    def scale_point(self, p, sx, sy, sz):

        """
        This function applies the scaling transformation matrix on the provided point.
        :param p: the original Point instance
        :param sx: scaling factor on the world's x axis
        :param sy: scaling factor on the world's y axis
        :param sz: scaling factor on the world's z axis
        :return: a Point instance
        """

        s1 = Vector(sx, 0, 0, 0)
        s2 = Vector(0, sy, 0, 0)
        s3 = Vector(0, 0, sz, 0)
        s4 = Vector(0, 0, 0, 1)

        Sxyz = Matrix(s1, s2, s3, s4)

        p = Sxyz.dot(p)

        return p









