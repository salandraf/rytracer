__author__ = 'Francesco'


from interfaces.GeometricObject import GeometricObject
import random as rnd
from numpy import inf, sqrt, interp
from utils.Constants import kEpsilon
from basic_elements.Vector import Vector
from basic_elements.Point import Point

class Triangle(GeometricObject):

    def __init__(self, v0, v1, v2, material):

        """
        This class implements the GeometricObject interface, inheriting all of its attributes
        and models a Triangle by specifying the following parameters:
        :param v0: a Point instance, think of it as a bottom-left point
        :param v1: a Point instance, think of it as a bottom.right point
        :param v2: a Point instance
        :param material: a Material instance
        """

        GeometricObject.__init__(self)

        self.v0 = v0
        self.v1 = v1
        self.v2 = v2
        self.n = self.normal(v0)
        self.material = material

        midp = (self.v1-self.v0)*0.5
        v2_midp = midp-v2
        norm = sqrt(v2_midp.dot(v2_midp))
        alfa = 2/3*norm
        dir = v2_midp.normalize()
        self.c = self.v2+(dir*alfa)


    def __iter__(self):
          for each in self.__dict__.keys():
              yield self.__getattribute__(each)

    def __eq__(self, other):
        """
        This functions determines if this instance of Triangle is equal to another instance of Triangle
        :param other: a Triangle instance
        :return: a boolean value.
        """
        return self.id == other.id if other != None else False


    def __str__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Triangle('+str(self.v0)+', '+str(self.v1)+', '+str(self.v2)+')'


    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Triangle('+str(self.v0)+', '+str(self.v1)+', '+str(self.v2)+')'


    def intersect(self, ray):

        """
        This function implements the ray-triangle intersection algorithm.
        :param ray: an instance of Ray
        :return: a float representing the alfa parameter to use in the equation of the ray-triangle intersection
        """

        normal = self.normal(ray.o)
        denom = ray.d.dot(normal)

        if denom == 0:
            return inf

        numer = (ray.o - self.v0).dot(normal)
        alfa = -numer / denom

        if alfa < kEpsilon:
            return inf

        '''calculation of the real intersection point'''
        ip = ray.o + (ray.d * alfa)

        '''we must check if the intersection point falls inside or outside the triangle'''
        if self.same_side_normal(ip):

            return alfa

        return inf


    def normal(self, pt):

        """
        This function calculates the normal to the surface based on a specific Point
        :param p: a Point instance
        :return: a Vector instance (the normal vector to the surface)
        """

        n = (self.v1-self.v0).cross_p(self.v2-self.v0).normalize()
        return n


    def same_side_normal(self, pt):

        """
        This functions checks if a givent point falls inside or outside the triangle using
        the normal vector's direction:
        if the normal vector has the same direction for each vertex of the triangle
        then the point falls inside the triangle
        else the point falls outside of it.
        :param pt: a Point instance
        :return: a boolean value indicatind if the point is inside or outside the triangle.
        """

        crp1 = (self.v1-self.v0).cross_p(pt-self.v0).normalize()
        crp2 = (self.v2-self.v1).cross_p(pt-self.v1).normalize()
        crp3 = (self.v0-self.v2).cross_p(pt-self.v2).normalize()

        scp1 = crp1.dot(crp2)
        scp2 = crp2.dot(crp3)
        scp3 = crp1.dot(crp3)

        return scp1 >= 0 and scp2 >= 0 and scp3 >= 0


    def aspect(self):

        """
        This function calculates the width and height of the triangle using its vertices.
        :return: a List object containing width and height
        """

        w = sqrt((self.v1 - self.v0).dot((self.v1 - self.v0)))
        h = sqrt((self.v2 - self.v0).dot((self.v2 - self.v0)))

        return [w, h]

    def get_width(self):

        """
        This functions returns the width of the triangle as its base.
        :return: a float number corresponding to the width of the triangle.
        """

        return sqrt((self.v1 - self.v0).dot((self.v1 - self.v0)))


    def get_height(self):

        """
        This functions returns the height of the triangle by calculating the area and the base of the triangle.
        The height is then calculated by resolving the standard formula area = (b*h)/2 using h as the unknown,
        h = (area*2)/b
        :return: a float number corresponding to the height of the triangle.
        """

        ab = self.v1-self.v0
        ac = self.v2-self.v0
        v_area = ab.cross_p(ac)
        area = sqrt(v_area.dot(v_area))*0.5
        base = sqrt(ab.dot(ab))
        height = (area*2)/float(base)

        return height


    def map_point_on_texture(self, p):

        """
        This function takes as input a point and calculates its coordinates in tangent space to perform texture mapping.
        :param p: a Point instance
        :return: u,v float coordinates in tangent space
        """

        '''the aspect of the plane is calculated to perform some scaling on the texture'''
        dim = self.aspect()
        w = dim[0]
        h = dim[1]
        aspect = float(w/float(h))
        repeatfor = self.material.texture.repeatfor if self.material.texture else self.material.bmap.repeatfor
        v0 = self.v0
        v1 = self.v1
        N = self.normal(p)

        u = 0
        v = 0

        '''in this case alfa is a vector whose norm determines the offset from the u,v origin in the tangent space'''
        alfa = (p - v0)*(1/float(sqrt((v1-v0).dot(v1-v0))))

        '''we must consider the orientation of the plane in order to get the right direction for the tangent space axes'''
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        if max_direction == abs(N.y):
            u = alfa.z*aspect*repeatfor
            v = alfa.x*repeatfor
        if max_direction == abs(N.x):
            u = alfa.z*repeatfor
            v = alfa.y*aspect*repeatfor
        if max_direction == abs(N.z):
            u = alfa.x*repeatfor
            v = alfa.y*aspect*repeatfor

        return u, v


    def sample_obj(self, num_samples):

        """
        This functions performs Monte Carlo Sampling on the object when the object is declared as a light object into
        the scene. This techniques is used when modeling soft shadows returning a list of points randomly taken
        to calculate the direction of shadow rays.
        :param num_samples: a number indicating the number of samples we want to use to perform sampling
        :return: a List object containing instances of Points
        """

        N = self.normal(self.v0)
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        w = self.get_width()
        h = self.get_height()
        points = []
        up = Vector(0.0, 0.0, 0.0)
        '''
        as before, we must consider the orientation of the Plane to get the right direction for the axis of the
        tangent space.
        '''
        if max_direction == abs(N.x):
            up = Vector(0.0, 1.0, 0.0)
        elif max_direction == abs(N.y):
            up = Vector(-1.0, 0.0, 0.0) if N.y > 0 else Vector(1.0, 0.0, 0.0)
        if max_direction == abs(N.z):
            up = Vector(0.0, 1.0, 0.0)

        u = up.cross_p(N)
        v = N.cross_p(u)

        '''For each sample we calculate a new random value in the tangent space directions'''
        for s in xrange(num_samples):

            du = rnd.uniform(-float(w/float(2)), float(w/float(2)))
            dv = rnd.uniform(-float(h/float(2)), float(h/float(2)))

            p = self.c + u * du + v * dv
            points.append(p)

        return points


    def perturbe_normal(self, p):

        """
        This function calculates a new normal vector to the object surface based on the normal map declared in the
        object's material. The new normal is calculated by using a perturbation vector which deviates the original
        direction of the normal.
        :param p: a Point instance
        :return: a Vector instance (the new normal vector)
        """

        '''
        we need to read the "color" of the normal map, which associates the [r, g, b] values to the [x, y, z]
        coordinates of the normal vector in that specific point
        '''
        u, v = self.map_point_on_texture(p)
        color = self.material.bmap.read(u, v)
        color[0] = interp(color[0], [0, 255], [-1, 1]) * self.material.bmap.factor
        color[1] = interp(color[1], [0, 255], [-1, 1]) * self.material.bmap.factor
        color[2] = interp(color[2], [0, 255], [-1, 1]) * self.material.bmap.factor

        N = self.normal(p)

        '''The orientation of the plane has to be considered as well'''
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        up = Vector(0.0, 0.0, 0.0)
        if max_direction == abs(N.x):
            up = Vector(0.0, 1.0, 0.0)
            color[0] = -color[0] if N.x <0 else color[0]
        elif max_direction == abs(N.y):
            up = Vector(-1.0, 0.0, 0.0)
            color[0] = -color[0] if N.y < 0 else color[0]
        elif max_direction == abs(N.z):
            up = Vector(0.0, 1.0, 0.0)

        udir = up.cross_p(N).normalize()
        vdir = N.cross_p(udir).normalize()

        '''Calculation of the perturbation vector D'''
        D = udir * color[0] + vdir * color[1] + N*color[2]

        '''Normal vector perturbation'''
        N = N + D
        return N.normalize()


    def read_texture(self, p):

        """
        This function retrieve the color of the texture associated with the object (if any) by calculating
        the u,v coordinates in tangent space and by calling the read() function on the Texture object itself.
        :param p: a Point instance
        :return: a List object containing the r, g, b values
        """

        u, v = self.map_point_on_texture(p)
        color = self.material.texture.read(u, v)
        return color





