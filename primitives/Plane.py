__author__ = 'Francesco'

from interfaces.GeometricObject import GeometricObject
import random as rnd
from numpy import sqrt, interp
from primitives.Triangle import Triangle
from basic_elements.Vector import Vector
from basic_elements.Matrix import Matrix
from basic_elements.Point import Point


class Plane(GeometricObject):

    def __init__(self, tl, bl, tr, br, material, down):

        """
        This class implements the GeometricObject interface, inheriting all of its attributes
        and models a Plane by specifying the following parameters:
        :param tl: top-left vertex,
        :param bl: bottom-left vertex,
        :param tr: top-right vertex,
        :param br: bottom-right vertex
        :param material: the material used to represent this geometric object,
        :param down: boolean value used to indicate if the plane normal should have opposite direction than
                     the usual (triangles are always built using ccw ordering of vertices,
                     in this way the normal points always "up")
        :return: a Plane object
        """

        GeometricObject.__init__(self)

        self.tl = tl
        self.tr = tr
        self.bl = bl
        self.br = br
        self.down = down

        '''if the normal to the plane has to be reversed it is necessary to change the "role" of the vertices'''
        if down:
            tl = self.tl
            tr = self.tr
            br = self.br
            bl = self.bl

            self.tl = tr
            self.bl = br
            self.tr = tl
            self.br = bl

        '''the Plane is composed using two triangles'''
        self.triA = Triangle(bl, br, tl, material)
        self.triB = Triangle(tr, tl, br, material)

        '''the normal to the plane is equal to the normal of one of its triangles (no matter which one)'''
        self.n = self.triA.normal(self.tl)
        N = self.n if not down else -self.n

        self.material = material

        '''
        to maintain consistency with the usage of an object as a light it is necessary to specify the center point
        whose coordinates may change based on the orientation of the plane. In this version of the project only axis-aligned
        planes have been considered.
        '''
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        if max_direction == abs(N.x):
            u = (self.br + self.bl).z * 0.5 if (self.br.z * self.bl.z) < 0.0 else (self.br - self.bl).z * 0.5
            v = (self.tl + self.bl).y * 0.5 if (self.tl.y * self.bl.y) < 0.0 else (self.tl - self.bl).y * 0.5
            self.c = Point(self.bl.x, v, u)
        elif max_direction == abs(N.y):
            u = (self.br + self.bl).z * 0.5 if (self.br.z * self.bl.z) < 0.0 else (self.br - self.bl).z * 0.5
            v = (self.tl + self.bl).x * 0.5 if (self.tl.x * self.bl.x) < 0.0 else (self.tl - self.bl).x * 0.5
            self.c = Point(v, self.bl.y, u)
        elif max_direction == abs(N.z):
            u = (self.br + self.bl).x * 0.5 if (self.br.x * self.bl.x) < 0.0 else (self.br - self.bl).x * 0.5
            v = (self.tl + self.bl).y * 0.5 if (self.tl.y * self.bl.y) < 0.0 else (self.tl - self.bl).y * 0.5
            self.c = Point(u, v, self.bl.z)


    def __eq__(self, other):

        """
        This functions determines if this instance of Plane is equal to another instance of Plane
        :param other: a Plane instance
        :return: a boolean value.
        """

        if isinstance(other, Plane):

            return self.id == other.id#self.tl == other.tl and self.bl == other.bl and self.tr ==other.tr and self.br == other.br and self.n == other.n


    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Plane {tl := '+str(self.tl)+', bl := '+str(self.bl)+', tr := '+str(self.tr)+', br := '+str(self.br)+', material := '+str(self.material)+'}'


    def normal(self, pt):

        """
        This function calculates the normal to the surface based on a specific Point
        :param pt: a Point instance
        :return: a Vector instance (the normal vector to the surface)
        """

        return self.triA.normal(self.bl) if not self.down else -self.triA.normal(self.bl)


    def intersect(self, ray):

        """
        This function is a wrapper used to recall the intersection over the triangles composing the Plane's surface.
        :param ray: an instance of Ray
        :return: a float representing the alfa parameter to use in the equation of the ray-plane intersection
        """

        triangles = [self.triA, self.triB]
        alfas = []
        for triangle in triangles:
            alfa = triangle.intersect(ray)
            alfas.append(alfa)
        min_alfa = min(alfas)
        index = alfas.index(min_alfa)
        self.itriangle = triangles[index]

        return min_alfa


    def same_side_normal(self, pt):

        """
        This function is a wrapper which recalls the same_side_normal() function defined over the triangles composing
        the Plane's surface, given as input a specific point.
        The purpose of this function is to test if the given point is inside or outside the Plane object.
        :param pt: a Point instance
        :return: a boolean value which indicates if the point is inside or outside the Plane object
        """

        ssn_A = self.triA.same_side_normal(pt)
        ssn_B = self.triB.same_side_normal(pt)

        return ssn_A and ssn_B


    def translate_points(self, dx, dy, dz):

        """
        This functions implements the translation transformation by taking as input the following parameters:
        :param dx: offset on the world x axis
        :param dy: offset on the world y axis
        :param dz: offset on the world z axis
        :return: void. The transformation takes place directly on the Point instances of the object.
        The offsets are used as input to the function translate_point which takes in a single Point instance and the offsets.
        """

        triangles = [self.triA, self.triB]
        for triangle in triangles:
            triangle.v0 = self.translate_point(triangle.v0, dx, dy, dz)
            triangle.v1 = self.translate_point(triangle.v1, dx, dy, dz)
            triangle.v2 = self.translate_point(triangle.v2, dx, dy, dz)

        self.tl = self.translate_point(self.tl, dx, dy, dz)
        self.bl = self.translate_point(self.bl, dx, dy, dz)
        self.tr = self.translate_point(self.tr, dx, dy, dz)
        self.br = self.translate_point(self.br, dx, dy, dz)


    def translate_point(self, p, dx, dy, dz):

        """
        This function applies the translation transformation matrix on the provided point.
        :param p: the original Point instance
        :param dx: offset on the world x axis
        :param dy: offset on the world y axis
        :param dz: offset on the world z axis
        :return: a Point instance
        """

        f1 = Vector(1, 0, 0, dx)
        f2 = Vector(0, 1, 0, dy)
        f3 = Vector(0, 0, 1, dz)
        f4 = Vector(0, 0, 0, 1)
        TR = Matrix(f1, f2, f3, f4)

        return TR.dot(p)


    def scale_points(self, sx, sy, sz):

        """
        This function implements the scaling transformation by taking as input the following parameters:
        :param sx: scaling factor on the world x axis
        :param sy: scaling factor on the world y axis
        :param sz: scaling factor on the world z axis
        :return: void - The transformation takes place directly on the Point instances of the object.
        The scaling factors are used as input to the function scale_point which takes in a single Point instance and
        the scaling factors.
        """

        triangles = [self.triA, self.triB]
        for triangle in triangles:
            triangle.v0 = self.scale_point(triangle.v0, sx, sy, sz)
            triangle.v1 = self.scale_point(triangle.v1, sx, sy, sz)
            triangle.v2 = self.scale_point(triangle.v2, sx, sy, sz)
        self.tl = self.scale_point(self.tl, sx, sy, sz)
        self.bl = self.scale_point(self.bl, sx, sy, sz)
        self.tr = self.scale_point(self.tr, sx, sy, sz)
        self.br = self.scale_point(self.br, sx, sy, sz)


    def scale_point(self, p, sx, sy, sz):

        """
        This function applies the scaling transformation matrix on the provided point.
        :param p: the original Point instance
        :param sx: scaling factor on the world x axis
        :param sy: scaling factor on the world y axis
        :param sz: scaling factor on the world z axis
        :return: a Point instance
        """

        s1 = Vector(sx, 0, 0, 0)
        s2 = Vector(0, sy, 0, 0)
        s3 = Vector(0, 0, sz, 0)
        s4 = Vector(0, 0, 0, 1)
        Sxyz = Matrix(s1, s2, s3, s4)

        p = Sxyz.dot(p)

        return p


    def map_point_on_texture(self, p):

        """
        This function takes as input a point and calculates its coordinates in tangent space to perform texture mapping.
        :param p: a Point instance
        :return: u,v float coordinates in tangent space
        """

        '''the aspect of the plane is calculated to perform some scaling on the texture'''
        dim = self.aspect()
        w = dim[0]
        h = dim[1]
        aspect = float(w/float(h))
        repeatfor = self.material.texture.repeatfor if self.material.texture else self.material.bmap.repeatfor
        v0 = self.triA.v0
        v1 = self.triA.v1
        N = self.normal(p)

        u = 0
        v = 0

        '''in this case alfa is a vector whose norm determines the offset from the u,v origin in the tangent space'''
        alfa = (p - v0)*(1/float(sqrt((v1-v0).dot(v1-v0))))

        '''we must consider the orientation of the plane in order to get the right direction for the tangent space axes'''
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        if max_direction == abs(N.y):
            u = alfa.z*aspect*repeatfor
            v = alfa.x*repeatfor
        if max_direction == abs(N.x):
            u = alfa.z*repeatfor
            v = alfa.y*aspect*repeatfor
        if max_direction == abs(N.z):
            u = alfa.x*repeatfor
            v = alfa.y*aspect*repeatfor
        return u, v


    def aspect(self):

        """
        This function calculates the width and height of the plane using its vertices.
        :return: a List object containing width and height
        """

        w = sqrt((self.triA.v1 - self.triA.v0).dot((self.triA.v1 - self.triA.v0)))
        h = sqrt((self.triA.v2 - self.triA.v0).dot((self.triA.v2 - self.triA.v0)))

        return [w, h]


    def get_width(self):

        """
        This functions returns the width of the plane by using bottom-left and bottom-right points.
        :return: a float number corresponding to the width of the plane.
        """

        return sqrt((self.br - self.bl).dot((self.br - self.bl)))


    def get_height(self):

        """
        This functions returns the height of the plane by using top-left and bottom-left points
        :return: a float number corresponding to the height of the plane.
        """

        return sqrt((self.tl - self.bl).dot((self.tl - self.bl)))


    def sample_obj(self, num_samples):

        """
        This functions performs Monte Carlo Sampling on the object when the object is declared as a light object into
        the scene. This techniques is used when modeling soft shadows returning a list of points randomly taken
        to calculate the direction of shadow rays.
        :param num_samples: a number indicating the number of samples we want to use to perform sampling
        :return: a List object containing instances of Points
        """

        N = self.normal(self.bl)
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        w = self.get_width()
        h = self.get_height()
        points = []
        up = Vector(0.0, 0.0, 0.0)
        '''
        as before, we must consider the orientation of the Plane to get the right direction for the axis of the
        tangent space.
        '''
        if max_direction == abs(N.x):
            up = Vector(0.0, 1.0, 0.0)
        elif max_direction == abs(N.y):
            up = Vector(-1.0, 0.0, 0.0) if N.y > 0 else Vector(1.0, 0.0, 0.0)
        if max_direction == abs(N.z):
            up = Vector(0.0, 1.0, 0.0)

        u = up.cross_p(N)
        v = N.cross_p(u)

        '''For each sample we calculate a new random value in the tangent space directions'''
        for s in xrange(num_samples):

            du = rnd.uniform(-float(w/float(2)), float(w/float(2)))
            dv = rnd.uniform(-float(h/float(2)), float(h/float(2)))

            p = self.c + u * du + v * dv
            points.append(p)

        return points


    def perturbe_normal(self, p):

        """
        This function calculates a new normal vector to the object surface based on the normal map declared in the
        object's material. The new normal is calculated by using a perturbation vector which deviates the original
        direction of the normal.
        :param p: a Point instance
        :return: a Vector instance (the new normal vector)
        """

        '''
        we need to read the "color" of the normal map, which associates the [r, g, b] values to the [x, y, z]
        coordinates of the normal vector in that specific point
        '''
        u, v = self.map_point_on_texture(p)
        color = self.material.bmap.read(u, v)
        color[0] = interp(color[0], [0, 255], [-1, 1]) * self.material.bmap.factor
        color[1] = interp(color[1], [0, 255], [-1, 1]) * self.material.bmap.factor
        color[2] = interp(color[2], [0, 255], [-1, 1]) * self.material.bmap.factor

        N = self.normal(p)

        '''The orientation of the plane has to be considered as well'''
        max_direction = max([abs(N.x), abs(N.y), abs(N.z)])
        up = Vector(0.0, 0.0, 0.0)
        if max_direction == abs(N.x):
            up = Vector(0.0, 1.0, 0.0)
            color[0] = -color[0] if N.x <0 else color[0]
        elif max_direction == abs(N.y):
            up = Vector(-1.0, 0.0, 0.0)
            color[0] = -color[0] if N.y < 0 else color[0]
        elif max_direction == abs(N.z):
            up = Vector(0.0, 1.0, 0.0)

        udir = up.cross_p(N).normalize()
        vdir = N.cross_p(udir).normalize()

        '''Calculation of the perturbation vector D'''
        D = udir * color[0] + vdir * color[1] + N*color[2]

        '''Normal vector perturbation'''
        N = N + D
        return N.normalize()


    def read_texture(self, p):

        """
        This function retrieve the color of the texture associated with the object (if any) by calculating
        the u,v coordinates in tangent space and by calling the read() function on the Texture object itself.
        :param p: a Point instance
        :return: a List object containing the r, g, b values
        """

        u, v = self.map_point_on_texture(p)
        color = self.material.texture.read(u, v)
        return color