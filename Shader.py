__author__ = 'Francesco'

import random
from utils.Constants import *
from Ray import Ray
from Cubemap import Cubemap
from basic_elements.Vector import Vector


class Shader:

    def __init__(self, ray, scene, ambient, camera, depth, soft_shadows_on, soft_shadow_samples, environment):
        """
        This class creates a Shader object, which is responsible for all the lighting and shading part of the rendering.
        The parameters used are the following:
        :param ray: a Ray object instance, is the current ray which has been cast in the scene
        :param scene: a List of GeometricObjects
        :param ambient: an Ambient object instance, represents the ambient light.
        :param camera: a Camera object instance
        :param depth: a number which indicates the depth of recursion for the shader to adopt in the tracing of a ray
        :param soft_shadows_on: a boolean value which enables the modeling of soft shadows. When set to false the scene
                                models only hard shadows.
        :param soft_shadows_samples: a number which indicates the number of samples to use when soft shadows are enabled.
        :param environment: a string which contains the relative path of the image to use for the skybox
                            (Cubemap object). If set to None the scene is rendered with black background color.

        :return: a Shader object instance
        """
        self.camera = camera
        self.lights = [obj for obj in scene if obj.isLight]
        self.scene = scene
        self.ambient = ambient
        self.ray = ray
        self.depth = depth
        self.soft_shadows_on = soft_shadows_on
        self.soft_shadow_samples = soft_shadow_samples
        self.environment = environment
        self.skybox = Cubemap(environment) if environment != None and environment != '' else None
        self.raw_scene = self.elaborate_scene(scene)


    def elaborate_scene(self, scene):

        """
        This function decompose complex objects in the scene to simplier objects (for example: Cube -> its Planes).
        :param scene: a List containing GeometricObject instances
        :return:a List containing GeometricObject instances
        """

        raw_scene = []
        for obj in scene:
            if hasattr(obj, 'faces'):
                faces = getattr(obj, 'faces')
                for face in faces:
                    raw_scene.append(face)
            else:
                raw_scene.append(obj)
        return raw_scene

    def shade(self):

        """
        This function calculates al the lighting and color values for the current ray
        :return: void. The shading modifies the color attribute of the Ray object
        """

        '''if the current ray intersects an object then we proceed with the shading'''
        if self.ray.i != inf_point:

            '''Ambient Light'''
            Ia = [0.0, 0.0, 0.0]
            if self.ray.iobj.material.ka > 0.0:
                Ia = self.ambient_component()

            '''Emissive Light'''
            Ie = [0.0, 0.0, 0.0]
            if self.ray.iobj.material.ke > 0.0:
                Ie = self.emissive_component()

            self.ray_color = [(self.ray.iobj.material.ka * Ia[k]) +
                              (self.ray.iobj.material.ke * Ie[k])
                              for k in xrange(len(self.ray.color))]

            '''Phong Lighting: Diffusive Light + Specular Light'''
            Id = [0.0, 0.0, 0.0]
            Is = [0.0, 0.0, 0.0]
            phong = [0.0, 0.0, 0.0]
            attenuationFactor = 1.0

            for light in self.lights:

                '''Casting Shadow Ray: this only in the case soft shadows are not enabled'''
                in_shadow = self.cast_shadows(light) if not self.soft_shadows_on \
                                                     else False

                distance = np.sqrt((self.ray.i - light.c).dot((self.ray.i - light.c)))
                light_intensity = min(1/float(1 + 0.01 * distance + 0.000010 * distance**2), 1.0)

                '''If point is in light, then we calculate diffusive and specular components of light'''

                Id = self.diffusive_component(light, Id)
                Is = self.specular_component(light, Is)

                phong = [(1.0-in_shadow)*light_intensity*((self.ray.iobj.material.kd*attenuationFactor*Id[i]) + (self.ray.iobj.material.ks*attenuationFactor*Is[i])) for i in xrange(len(self.ray.color))]

                '''If soft shadows are enabled and the ray is not intersecting a light object'''
                if self.soft_shadows_on and not self.ray.iobj.isLight:
                    attenuationFactor = self.cast_soft_shadow(light)
                    phong = [(1.0-in_shadow)*light_intensity*((self.ray.iobj.material.kd*attenuationFactor*Id[i]) + (self.ray.iobj.material.ks*attenuationFactor*Is[i])) for i in xrange(len(self.ray.color))]





            '''Reflected Rays (if material is reflective)'''
            #self.ray.color = [self.ray.color[k] + self.ray.iobj.material.kd*Id[k] + self.ray.iobj.material.ks * Is[k] for k in xrange(len(self.ray.color))]
            reflection = [0.0, 0.0, 0.0]
            if self.ray.iobj.material.kr > 0.0:
                if self.depth > 0:
                    reflection = self.reflect_ray()
                    #self.ray.color = [self.ray.color[k] + reflection[k] for k in xrange(len(self.ray.color))]

            '''Glossy Reflected Rays (if material is glossy reflective)'''
            glossy_r = [0.0, 0.0, 0.0]
            if self.ray.iobj.material.kgr > 0.0:
                if self.depth > 0:
                    glossy_r = self.reflect_ray_glossy()
                    #self.ray.color = [self.ray.color[k] + reflection[k] for k in xrange(len(self.ray.color))]

            '''Transmitted Rays (if material is transparent)'''
            refraction = [0.0, 0.0, 0.0]
            if self.ray.iobj.material.kt > 0.0:
                if self.depth > 0:
                    #print '################### REFRACTION '+str(self.depth)+'#########################'
                    refraction = self.transmit_ray()
                    #self.ray.color = [self.ray.color[k] + refraction[k] for k in xrange(len(self.ray.color))]
            #attenuation = [light.material.color[i] * attenuationFactor  for i in range(len(self.ray.color))]

            '''Glossy Transmitted Rays (if material is glossy transparent)'''
            glossy_t = [0.0, 0.0, 0.0]
            if self.ray.iobj.material.kgt > 0.0:
                if self.depth > 0:
                    glossy_t = self.transmit_ray_glossy()


            """Sum of light components"""
            self.ray.color = [self.ray.color[k] *
                              (1.0 - self.ray.iobj.material.kr) *
                              (1.0 - self.ray.iobj.material.kt) *
                              (1.0 - self.ray.iobj.material.kgr) *
                              (1.0 - self.ray.iobj.material.kgt) +
                              phong[k] +
                              (self.ray.iobj.material.kr*reflection[k]) +
                              (self.ray.iobj.material.kgr*glossy_r[k]) +
                              (self.ray.iobj.material.kt*refraction[k]) +
                              (self.ray.iobj.material.kgt*glossy_t[k])
                              for k in xrange(len(self.ray.color))] if not self.ray.iobj.isLight \
                                                                    else self.ray.color

        else:
            '''if the current ray does not intersect any object in the scene, then we use the skybox color (if any) else we use black background color'''
            self.ray.color = self.skybox.readCubeMap(self.ray.d) if self.skybox != None \
                                                                 else [0.0, 0.0, 0.0]

            #self.ray.color = self.readCubeMap() if self.environment != None and self.environment != '' and self.environment != 'None' else self.ray.color


    def cast_shadows(self, light):

        """
        This functions casts a shadow ray for a given light.
        :param light: a GeometricObject instance declared as light
        :return: a boolean value to determine if the current intersection point for the primary ray is in shadow or not
        """

        dir = (light.c - self.ray.i).normalize()
        shadow_ray = Ray(self.ray.i, dir)
        shadow_ray.type = 'shadow'
        shadow_ray.intersect(self.raw_scene)

        '''
        Computing distance light-intersection point (dli)
        and distance shadow ray intersection point-intersection point (dsi) is required in order to determine
        if the intersection point of the shadow ray is nearer than the light centre.
        '''
        dli = (light.c - self.ray.i).dot(light.c - self.ray.i)
        dsi = (shadow_ray.i - self.ray.i).dot(shadow_ray.i - self.ray.i)
        #in_shadow = True if shadow_ray.i != inf_point and not shadow_ray.iobj.isLight and dsi < dli \
        #                 else False
        shadow_attenuation = 1 if shadow_ray.i != inf_point and not shadow_ray.iobj.isLight and dsi < dli \
                               else 0
        shadow_attenuation = ((1.0 - shadow_ray.iobj.material.kr) * shadow_attenuation) if shadow_ray.iobj.material.kt > 0.0 else shadow_attenuation

        return shadow_attenuation


    def cast_soft_shadow(self, light):

        """
        This function cast a shadow ray for each point sampled on the light object using monte carlo sampling.
        The technique is the same as for the standard shadow ray computed from the cast_shadows() function.
        The final color on the current intersection point depends on the coefficient (coef) which indicates the ratio
        of casted shadow rays which reach the light on the total number of shadow rays casted.
        :param light: a GeometricObject instance declared as light
        :return: a float, resulting from the ratio of non-shadow rays on the total number of rays.
        """

        hits = 0.0
        num_samples = self.soft_shadow_samples
        points = light.sample_obj(num_samples)
        shadow_ray = Ray(self.ray.i, self.camera.w)
        for p in points:
            d = (p - self.ray.i).normalize()
            shadow_ray.d = d
            shadow_ray.intersect(self.raw_scene)
            '''
            a counter for the number of rays which hit a light is incremented every time the ray is not blocked from
            another object in the scene
            '''
            if (shadow_ray.iobj == light) or (shadow_ray.iobj == self.ray.iobj):
                hits += 1.0

        coef = float(hits)/float(len(points))
        #self.ray.color = [self.ray.color[i] + (self.ray.color[i] * coef) for i in xrange(len(self.ray.color))]
        return coef


    def ambient_component(self):

        """
        This function computes the color contribution of the ambient light
        :return: a List containing the r, g, b values for the color of the current intersection point
        """

        obj_color = self.ray.iobj.material.color if not self.ray.iobj.material.texture \
                                                 else self.ray.iobj.read_texture(self.ray.i)

        color = [self.ambient.color[k] * obj_color[k] for k in range(len(self.ray.color))]
        return color


    def emissive_component(self):

        """
        This function computes the color contribution of the emissive light
        :return: a List containing the r, g, b values for the color of the current intersection point
        """

        obj_color = self.ray.iobj.material.color if not self.ray.iobj.material.texture \
                                                 else self.ray.iobj.read_texture(self.ray.i)

        color = [obj_color[k] for k in range(len(self.ray.color))]
        return color


    def diffusive_component(self, light, Id):

        """
        This function computes the color contribution of the diffusive light
        :param light: a GeometricObject instance, declared as light
        :param Id: a List containing the r, g, b values for the current value of diffusive light.
                    this variable accumulates the contribution from every source of light present in the scene.
        :return: a List containing the r, g, b values for the current diffusive light color
                 at the current intersection point
        """

        '''L is the versor direct from the intersection point towards the light source'''
        L = (light.c - self.ray.i).normalize()

        '''
        Calculus of the normal vector: the normal is perpendicular to the geometric surface of the object if
        there is no normal map defined for the current intersected object, else the perturbated normal is calculated
        on the basis of the normal map
        '''
        N = self.ray.iobj.normal(self.ray.i) if self.ray.iobj.material.bmap == None \
                                             else self.ray.iobj.perturbe_normal(self.ray.i)

        '''Calculus of the angle between the normal vector and the light-directed vector'''
        costheta = max(N.dot(L), 0) if N != None \
                                    else 0


        Id = [Id[k] + ((light.material.color[k]*light.light_intensity) * costheta) for k in xrange(len(self.ray.color))]
        #self.ray.color = [self.ray.color[i] + (Id[i] * self.ray.iobj.material.kd) for i in xrange(len(color))] if not self.ray.iobj.isLight else self.ray.color
        return Id


    def specular_component(self, light, Is):

        """
        This function computes the color contribution of the specular light using Phong shading model
        :param light: a GeometricObject instance, declared as light
        :param Is: a List containing the r, g, b values for the current value of specular light.
                    this variable accumulates the contribution from every source of light present in the scene.
        :return: a List containing the r, g, b values for the current specular light color
                 at the current intersection point
        """

        '''L is the versor direct from the intersection point towards the light source'''
        L = (light.c - self.ray.i).normalize()

        '''
        Calculus of the normal vector: the normal is perpendicular to the geometric surface of the object if
        there is no normal map defined for the current intersected object, else the perturbated normal is calculated
        on the basis of the normal map
        '''
        N = self.ray.iobj.normal(self.ray.i) if self.ray.iobj.material.bmap == None \
                                             else self.ray.iobj.perturbe_normal(self.ray.i)

        '''Calculus of the angle between the normal vector and the light-directed vector'''
        costheta = max(N.dot(L), 0) if N != None else 0

        '''Phong model for reflected light'''
        Vn = (self.camera.eye - self.ray.i).normalize()
        R = (N * (2 * costheta)) - L if N != None else Vector(0, 0, 0)
        Rn = R.normalize()
        cosAlpha = max(0, Rn.dot(Vn)) ** self.ray.iobj.material.sh

        Is = [Is[k] + ((light.material.color[k]*light.light_intensity) * cosAlpha) for k in xrange(len(self.ray.color))]
        #self.ray.color = [self.ray.color[i] + (Is[i] * self.ray.iobj.material.ks) for i in xrange(len(color))] if not self.ray.iobj.isLight else self.ray.color

        return Is


    def reflect_ray(self):

        """
        This function is used when the object intersected from the current ray is made of a reflective material
        :return:a List containing the r, g, b values for the reflected ray's intersection point
        """

        '''
        Calculus of the normal vector: the normal is perpendicular to the geometric surface of the object if
        there is no normal map defined for the current intersected object, else the perturbated normal is calculated
        on the basis of the normal map
        '''
        N = self.ray.iobj.normal(self.ray.i) if self.ray.iobj.material.bmap == None \
                                             else self.ray.iobj.perturbe_normal(self.ray.i)

        rdir = (self.ray.d - (N * 2.0 * self.ray.d.dot(N))).normalize() if N != None \
                                                                        else Vector(0, 0, 0)

        spec_ray = Ray(self.ray.i, rdir)
        spec_ray.intersect(self.raw_scene)

        '''Recursive call to Shader object'''
        shader = Shader(spec_ray, self.scene, self.ambient, self.camera, self.depth-1, self.soft_shadows_on, self.soft_shadow_samples, self.environment)
        shader.shade()
        #current_ray_color = self.ray.color
        #self.ray = spec_ray
        #spec_ray.color = current_ray_color
        #self.depth = self.depth -1
        color = [spec_ray.color[k] for k in range(len(self.ray.color))]
        #self.ray.color = [self.ray.color[i] + (color[i] * self.ray.iobj.material.kr) for i in xrange(len(color))]
        return color


    def reflect_ray_glossy(self):

        """
        This function is used when the object intersected from the current ray is made of a glossy reflective material
        :return:a List containing the mean r, g, b values for the reflected rays's intersection points
        """

        '''
        Calculus of the normal vector: the normal is perpendicular to the geometric surface of the object if
        there is no normal map defined for the current intersecated object, else the perturbated normal is calculated
        on the basis of the normal map
        '''
        N = self.ray.iobj.normal(self.ray.i) if self.ray.iobj.material.bmap == None \
                                             else self.ray.iobj.perturbe_normal(self.ray.i)

        rdir = (self.ray.d - (N * 2.0 * self.ray.d.dot(N))).normalize() if N != None \
                                                                        else Vector(0, 0, 0)

        '''
        Is necessary to calculate the tangent space versors to cover the case in which we are using a normal map,
        because we don't know what direction will have the normal. This way we can proceed indipendently from the
        fact that the normal is deviated from the "geometric" normal.
        '''
        udir = rdir.cross_p(N).normalize()
        vdir = udir.cross_p(rdir).normalize()
        color = [0.0, 0.0, 0.0]
        sample = self.ray.iobj.material.kgrs
        sqrt_sample = np.sqrt(sample)

        spec_ray = Ray(self.ray.i, rdir)
        shader = Shader(spec_ray, self.scene, self.ambient, self.camera, self.depth-1, self.soft_shadows_on, self.soft_shadow_samples, self.environment)

        '''for each sample we use Monte Carlo sampling to get random values on the u-v directions'''
        for i in xrange(sample):
            uoffset = random.uniform(-1/float(sqrt_sample), 1/float(sqrt_sample))
            voffset = random.uniform(-1/float(sqrt_sample), 1/float(sqrt_sample))
            rdir = (rdir + (udir * uoffset) + (vdir * voffset)).normalize()
            spec_ray.d = rdir
            spec_ray.intersect(self.raw_scene)
            '''Recursive call to Shader object'''
            shader.ray = spec_ray
            shader.shade()
            '''each iteration produces a color which contributes for 1/#samples to the total final color'''
            color = [color[k]+spec_ray.color[k]*(1/float(sample)) for k in range(len(self.ray.color))]
        #self.ray.color = [self.ray.color[i] + (color[i] * self.ray.iobj.material.kgr) for i in xrange(len(color))]
        return color


    def transmit_ray(self):

        """
        This function is used when the object intersected from the current ray is made of a transmissive material
        :return:a List containing the r, g, b values for the transmitted ray's intersection point
        """

        '''
        Calculus of the normal vector: the normal is perpendicular to the geometric surface of the object if
        there is no normal map defined for the current intersected object, else the perturbated normal is calculated
        on the basis of the normal map
        '''
        N = self.ray.iobj.normal(self.ray.i) if self.ray.iobj.material.bmap == None \
                                             else self.ray.iobj.perturbe_normal(self.ray.i)

        I = self.ray.d
        n1 = 1.0
        n2 = self.ray.iobj.material.ior if self.ray.iobj.material.ior > 0.0 \
                                        else 1.0

        n = n1/n2
        cosI = N.dot(I) if N != None \
                        else 0

        '''Entering into the object'''
        if cosI < 0.0:
            cosI = -1 * N.dot(I) if N != None \
                                 else 0

        elif cosI > 0.0:
            '''Exiting from the object'''
            n = n2/n1
            cosI = N.dot(I) if N != None \
                            else 0
            N = -N

        '''Schlick's approximation to Fresnel equation'''
        R0 = ((n1-n2)/float(n1+n2))**2

        radix_arg = 1.0 - (n**2 * (1.0 - cosI**2))
        '''
        Total inner reflection: in this case calling the reflect_ray() function prevents from obtaining a black
        contouring on the borders of the object.
        '''
        if radix_arg <= 0.0:

            color = self.reflect_ray()

            return color

        cosT = np.sqrt(radix_arg)

        self.ray.iobj.material.kr = R0 + ((1 - R0)*((1 - cosI)**5)) if n1 <= n2 \
                                                else R0 + ((1 - R0)*((1 - cosT)**5))



        tdir = (I*n + N*(n * cosI - cosT)).normalize() if N != None \
                                                       else Vector(0, 0, 0)

        trans_ray = Ray(self.ray.i, tdir)
        trans_ray.intersect(self.raw_scene)
        '''Recursive call to Shader object'''
        shader = Shader(trans_ray, self.scene, self.ambient, self.camera, self.depth-1, self.soft_shadows_on, self.soft_shadow_samples, self.environment)
        shader.shade()
        color = [trans_ray.color[k] for k in range(len(self.ray.color))]
        #self.ray.color = [self.ray.color[i]*(1.0 - self.ray.iobj.material.kt) + (color[i] * self.ray.iobj.material.kt) for i in xrange(len(color))]
        return color


    def transmit_ray_glossy(self):

        """
        This function is used when the object intersected from the current ray is made of a glossy transmissive material
        :return:a List containing the mean r, g, b values for the transmitted rays's intersection points
        """

        '''
        Calculus of the normal vector: the normal is perpendicular to the geometric surface of the object if
        there is no normal map defined for the current intersected object, else the perturbated normal is calculated
        on the basis of the normal map
        '''
        N = self.ray.iobj.normal(self.ray.i) if self.ray.iobj.material.bmap == None \
                                             else self.ray.iobj.perturbe_normal(self.ray.i)

        I = self.ray.d
        n1 = 1.0
        n2 = self.ray.iobj.material.ior if self.ray.iobj.material.ior > 0.0 \
                                        else 1.0
        n = n1/n2
        cosI = N.dot(I) if N != None \
                        else 0

        '''Entering into the object'''
        if cosI < 0.0:
            cosI = -1 * N.dot(I) if N != None \
                                 else 0

        elif cosI > 0.0:
            '''Exiting from the object'''
            n = n2/n1
            cosI = N.dot(I) if N != None \
                            else 0
            N = -N

        '''Schlick's approximation to Fresnel equation'''
        R0 = ((n1-n2)/float(n1+n2))**2

        radix_arg = 1.0 - (n**2 * (1.0 - cosI**2))
        '''
        Total inner reflection: in this case calling the reflect_ray() function prevents from obtaining a black
        contouring on the borders of the object.
        '''
        if radix_arg <= 0.0:
            color = self.reflect_ray()
            return color


        cosT = np.sqrt(radix_arg)

        self.ray.iobj.material.kr = R0 + ((1 - R0)*((1 - cosI)**5)) if n1 <= n2 \
                                                else R0 + ((1 - R0)*((1 - cosT)**5))

        tdir = (I*n + N*(n * cosI - cosT)).normalize() if N != None \
                                                       else Vector(0, 0, 0)

        '''
        Is necessary to calculate the tangent space versors to cover the case in which we are using a normal map,
        because we don't know what direction will have the normal. This way we can proceed indipendently from the
        fact that the normal is deviated from the "geometric" normal.
        '''
        udir = N.cross_p(tdir).normalize()
        vdir = N.cross_p(udir).normalize()
        color = [0.0, 0.0, 0.0]
        sample = self.ray.iobj.material.kgts
        sqrt_sample = np.sqrt(sample)

        '''for each sample we use Monte Carlo sampling to get random values on the u-v directions'''
        for i in xrange(sample):
            uoffset = random.uniform(-1/float(sqrt_sample), 1/float(sqrt_sample))
            voffset = random.uniform(-1/float(sqrt_sample), 1/float(sqrt_sample))
            tdir = (tdir + (udir * uoffset) + (vdir * voffset)).normalize()
            trans_ray = Ray(self.ray.i, tdir)
            trans_ray.intersect(self.raw_scene)
            '''Recursive call to Shader object'''
            shader = Shader(trans_ray, self.scene, self.ambient, self.camera, self.depth-1, self.soft_shadows_on, self.soft_shadow_samples, self.environment)
            shader.shade()
            '''each iteration produces a color which contributes for 1/#samples to the total final color'''
            color = [color[k] + trans_ray.color[k]*(1/float(sample)) for k in range(len(self.ray.color))]
        #self.ray.color = [self.ray.color[i]*(1.0 - self.ray.iobj.material.kgt) + (color[i] * self.ray.iobj.material.kgt) for i in xrange(len(color))]
        return color


    '''def readCubeMap(self):

        color = [0.0, 0.0, 0.0]
        cm = Cubemap(self.environment)
        d = self.ray.d
        max_d = max(abs(d.x), abs(d.y), abs(d.z))

        face = None
        u = 0
        v = 0
        if abs(d.x) == max_d:
            if d.x > 0.0:
                u = (d.z/d.x+1.0)*0.5
                v = 1.0 - (d.y/d.x+1.0)*0.5
                face = cm.back
                #print 'back'
                color = self.readTexture(face, u, v, face.shape[1], face.shape[0])
            elif d.x < 0.0:
                u = (d.z/d.x+1.0)*0.5
                v = (d.y/d.x+1.0)*0.5
                face = cm.front
                #print 'front'
                color = self.readTexture(face, u, v, face.shape[1], face.shape[0])
        elif abs(d.y) == max_d:
            if d.y > 0.0:
                u = 1.0 - (d.z/d.y+1.0)*0.5
                v = 1.0 - (d.x/d.y+1.0)*0.5
                face = cm.top
                #print 'top'
                color = self.readTexture(face, u, v, face.shape[1], face.shape[0])
            elif d.y < 0.0:
                u = (d.z/d.y+1.0)*0.5
                v = 1.0 - (d.x/d.y+1.0)*0.5
                face = cm.down
                #print 'down'
                color = self.readTexture(face, u, v, face.shape[1], face.shape[0])
        elif abs(d.z) == max_d:
            if d.z > 0.0:
                u = 1.0 - (d.x/d.z+1.0)*0.5
                v = 1.0 - (d.y/d.z+1.0)*0.5
                face = cm.left
                #print 'left'
                color = self.readTexture(face, u, v, face.shape[1], face.shape[0])
            elif d.z < 0.0:
                u = 1.0 - (d.x/d.z+1.0)*0.5
                v = (d.y/d.z+1.0)*0.5
                face = cm.right
                #print 'right'
                color = self.readTexture(face, u, v, face.shape[1], face.shape[0])

        return color


    def readTexture(self, img, u, v, sizeU, sizeV):

        b, g, r = cv2.split(img)
        u = abs(u)
        v = abs(v)

        x = (u)*sizeU
        y = (v)*sizeV

        color_r = r[y,x]
        color_g = g[y,x]
        color_b = b[y,x]

        return [color_r, color_g, color_b]'''








