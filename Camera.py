__author__ = 'Francesco'


import numpy as np
from basic_elements.Matrix import Matrix
from basic_elements.Point import Point
from basic_elements.Vector import Vector


class Camera:

    def __init__(self, p, lookat, up, focal_length, f_number):

        """
        This class models a Camera object. The camera is created by using the
        following parameters:
        :param p: a Point instance, the point is used to place the camera in the world
        :param lookat: a Point instance, the point in the world where the camera is pointing to
        :param up: a Vector instance, used to take an approximation of the up direction for the camera
        :param focal_length: a float number, used to simulate the focal length of the camera lens
        :param f_number: a float number, used to simulate the diaphram aperture of the camera lens
        """

        self.eye = p
        self.lookat = lookat
        self.focal_length = focal_length
        self.f_number = f_number

        '''Calculation of Camera space coordinate'''
        self.w =(self.eye - lookat).normalize()
        print self.w
        self.u = up.cross_p(self.w).normalize()
        print self.u
        self.v = self.w.cross_p(self.u).normalize()
        print self.v


    def __str__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Camera = { w := '+str(self.w)+', u := '+str(self.u)+', v := '+str(self.v)


    def transform(self, p):

        """
        This functions implements the camera transform such that given an input point
        p expressed in world coordinates, it gets transformed in camera coordinates.
        :param p: a Point instance
        :return: a Point instance
        """

        inf_point = Point(np.inf, np.inf, np.inf)

        '''
        if the given point is not located at infinite then the transform is
        calculated else the "infinite" point is returned
        '''
        if p < inf_point:

            t1 = Vector(1, 0, 0, -self.eye.x)
            t2 = Vector(0, 1, 0, -self.eye.y)
            t3 = Vector(0, 0, 1, -self.eye.z)
            t4 = Vector(0, 0, 0, 1)
            TR = Matrix(t1, t2, t3, t4)

            r1 = Vector(self.u.x, self.u.y, self.u.z, 0)
            r2 = Vector(self.v.x, self.v.y, self.v.z, 0)
            r3 = Vector(self.w.x, self.w.y, self.w.z, 0)
            r4 = Vector(0, 0, 0, 1)

            ROT = Matrix(r1, r2, r3, r4)

            CAMERA_TRANSFORM = ROT.dot(TR)

            return CAMERA_TRANSFORM.dot(p)
        else:
            return inf_point


    def world_transform(self, p):

        """
        This functions implements the inverse of the camera transform such that given an input point
        p expressed in camera coordinates, it gets transformed in world coordinates.
        :param p: a Point instance
        :return: a Point instance
        """

        inf_point = Point(np.inf, np.inf, np.inf)
        '''
        if the given point is not located at infinite then the transform is
        calculated else the "infinite" point is returned
        '''
        if p < inf_point:

            t1 = Vector(1, 0, 0, self.eye.x)
            t2 = Vector(0, 1, 0, self.eye.y)
            t3 = Vector(0, 0, 1, self.eye.z)
            t4 = Vector(0, 0, 0, 1)
            TR = Matrix(t1, t2, t3, t4)


            t1 = np.array([1, 0, 0, self.eye.x])
            t2 = np.array([0, 1, 0, self.eye.y])
            t3 = np.array([0, 0, 1, self.eye.z])
            t4 = np.array([0, 0, 0, 1])
            TR = np.matrix([t1, t2, t3, t4])


            r1 = Vector(self.u.x, self.u.y, self.u.z, 0)
            r2 = Vector(self.v.x, self.v.y, self.v.z, 0)
            r3 = Vector(-self.w.x, -self.w.y, -self.w.z, 0)
            #r3 = Vector(self.n.x, self.n.y, self.n.z, 0)
            r4 = Vector(0, 0, 0, 1)


            r1 = np.array([self.u.x, self.u.y, self.u.z, 0])
            r2 = np.array([self.v.x, self.v.y, self.v.z, 0])
            r3 = np.array([-self.w.x, -self.w.y, -self.w.z, 0])
            r4 = np.array([0, 0, 0, 1])

            ROT = np.matrix([r1, r2, r3, r4]).transpose()

            WORLD_TRANSFORM = ROT.dot(TR.T)

            tr_p = np.squeeze(WORLD_TRANSFORM.dot(np.array([p.x, p.y, p.z, p.w])).tolist())
            p = Point(tr_p[0], tr_p[1], tr_p[2])
            return p

        else:

            return inf_point


