__author__ = 'Francesco'

from basic_elements.Point import Point
from basic_elements.Vector import Vector


class Matrix:

    def __init__(self, v1, v2, v3, v4):

        """
        This class models a 4 dimensional square Matrix object by taking as input
        four vectors expressed in homogeneous coordinates:
        :param v1: a Vector instance, used as row vector
        :param v2: a Vector instance, used as row vector
        :param v3: a Vector instance, used as row vector
        :param v4: a Vector instance, used as row vector
        :return: a Matrix object instance
        """

        self.row1 = v1
        self.row2 = v2
        self.row3 = v3
        self.row4 = v4

    def __add__(self, other):

        """
        This function implements the sum element by element with another Matrix object or with a scalar
        :param other: a Matrix instance or a number
        :return: a Matrix instance
        """

        if isinstance(other, Matrix):
            return Matrix(self.row1+other.row1, self.row2+other.row2, self.row3+other.row3, self.row4+other.row4)
        if isinstance(other, Vector):
            raise TypeError
        else:
            return Matrix(self.row1+other, self.row2+other, self.row3+other, self.row4+other)

    def __sub__(self, other):

        """
        This function implements the difference element by element with another Matrix object or with a scalar
        :param other: a Matrix instance or a scalar
        :return: a Matrix instance
        """

        if isinstance(other, Matrix):
            return Matrix(self.row1-other.row1, self.row2-other.row2, self.row3-other.row3, self.row4-other.row4)

        if isinstance(other, Vector):
            raise TypeError
        else:
            return Matrix(self.row1-other, self.row2-other, self.row3-other, self.row4-other)

    def dot(self, m2):

        """
        This function implements the product rows per columns with another Matrix object or with a scalar
        :param m2: a Matrix or a Vector or a Point instance
        :return: a Matrix or a Vector or a Point instance
        """

        if isinstance(m2, Matrix):

            a1 = self.row1.x*m2.row1.x + self.row1.y*m2.row2.x + self.row1.z*m2.row3.x + self.row1.w*m2.row4.x
            a2 = self.row1.x*m2.row1.y + self.row1.y*m2.row2.y + self.row1.z*m2.row3.y + self.row1.w*m2.row4.y
            a3 = self.row1.x*m2.row1.z + self.row1.y*m2.row2.z + self.row1.z*m2.row3.z + self.row1.w*m2.row4.z
            a4 = self.row1.x*m2.row1.w + self.row1.y*m2.row2.w + self.row1.z*m2.row3.w + self.row1.w*m2.row4.w

            b1 = self.row2.x*m2.row1.x + self.row2.y*m2.row2.x + self.row2.z*m2.row3.x + self.row2.w*m2.row4.x
            b2 = self.row2.x*m2.row1.y + self.row2.y*m2.row2.y + self.row2.z*m2.row3.y + self.row2.w*m2.row4.y
            b3 = self.row2.x*m2.row1.z + self.row2.y*m2.row2.z + self.row2.z*m2.row3.z + self.row2.w*m2.row4.z
            b4 = self.row2.x*m2.row1.w + self.row2.y*m2.row2.w + self.row2.z*m2.row3.w + self.row2.w*m2.row4.w

            c1 = self.row3.x*m2.row1.x + self.row3.y*m2.row2.x + self.row3.z*m2.row3.x + self.row3.w*m2.row4.x
            c2 = self.row3.x*m2.row1.y + self.row3.y*m2.row2.y + self.row3.z*m2.row3.y + self.row3.w*m2.row4.y
            c3 = self.row3.x*m2.row1.z + self.row3.y*m2.row2.z + self.row3.z*m2.row3.z + self.row3.w*m2.row4.z
            c4 = self.row3.x*m2.row1.w + self.row3.y*m2.row2.w + self.row3.z*m2.row3.w + self.row3.w*m2.row4.w

            d1 = self.row4.x*m2.row1.x + self.row4.y*m2.row2.x + self.row4.z*m2.row3.x + self.row4.w*m2.row4.x
            d2 = self.row4.x*m2.row1.y + self.row4.y*m2.row2.y + self.row4.z*m2.row3.y + self.row4.w*m2.row4.y
            d3 = self.row4.x*m2.row1.z + self.row4.y*m2.row2.z + self.row4.z*m2.row3.z + self.row4.w*m2.row4.z
            d4 = self.row4.x*m2.row1.w + self.row4.y*m2.row2.w + self.row4.z*m2.row3.w + self.row4.w*m2.row4.w

            v1 = Vector(a1, a2, a3, a4)
            v2 = Vector(b1, b2, b3, b4)
            v3 = Vector(c1, c2, c3, c4)
            v4 = Vector(d1, d2, d3, d4)

            return Matrix(v1, v2, v3, v4)

        elif isinstance(m2, Vector):

            x = self.row1.dot(m2)
            y = self.row2.dot(m2)
            z = self.row3.dot(m2)
            w = self.row4.dot(m2)

            if w != 1:
                x = x/w
                y = y/w
                z = z/w
                w = w/w

            return Vector(x, y, z, w)

        elif isinstance(m2, Point):

            x = self.row1.dot(m2)
            y = self.row2.dot(m2)
            z = self.row3.dot(m2)
            w = self.row4.dot(m2)

            return Point(x, y, z, w)


    def transpose(self):
        """
        This function implements the transposition of the elements for the current matrix
        :return: a Matrix instance
        """

        v1 = Vector(self.row1.x, self.row2.x, self.row3.x, self.row4.x)
        v2 = Vector(self.row1.y, self.row2.y, self.row3.y, self.row4.y)
        v3 = Vector(self.row1.z, self.row2.z, self.row3.z, self.row4.z)
        v4 = Vector(self.row1.w, self.row2.w, self.row3.w, self.row4.w)

        return Matrix(v1, v2, v3, v4)

    def __str__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Matrix('+str(self.row1)+'\n '+str(self.row2)+'\n '+str(self.row3)+'\n '+str(self.row4)+')'

    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Matrix('+self.row1+'\n '+self.row2+'\n '+self.row3+'\n '+self.row4+')'

