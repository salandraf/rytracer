__author__ = 'Francesco'


class Point:


    def __init__(self, x, y, z, *args):

        """
        This class models a 3d Point expressed in homogeneous coordinates (so that it has
        4 components) by taking as input the following parameters:
        :param x: a float which stands for the x coordinate in a given coordinate system
        :param y: a float which stands for the y coordinate in a given coordinate system
        :param z: a float which stands for the z coordinate in a given coordinate system
        :param args: a float which stands for the last coordinate (optional)
        :return: a Point instance
        """

        self.x = x
        self.y = y
        self.z = z
        self.w = 1

        if len(args) > 0:
            self.w = args[0]


    def __add__(self, other):

        """
        This function implements the sum Point-Point or Point-Vector or Point-scalar
        :param other: a Point or Vector instance or a scalar number
        :return: a Point instance
        """

        from basic_elements.Vector import Vector
        if isinstance(other, Point):
            return Point(self.x+other.x, self.y+other.y, self.z+other.z)

        elif isinstance(other, Vector):
            return Point(self.x+other.x, self.y+other.y, self.z+other.z)

        else:
            return Point(self.x+other, self.y+other, self.z+other)


    def __sub__(self, other):

        """
        This function implements the difference Point-Point or Point-scalar
        :param other: a Point instance or a scalar number
        :return: a Point or a Vector instance
        """

        from basic_elements.Vector import Vector

        if isinstance(other, Point):
            '''the difference between teo Points is supposed to produce a Vector'''
            return Vector(self.x-other.x, self.y-other.y, self.z-other.z)

        elif isinstance(other, int) or isinstance(other, float) or isinstance(other, long):
            return Point(self.x-other, self.y-other, self.z-other)


    def __mul__(self, other):

        """
        This function implements the product Point-Point or Point-Vector or Point-scalar
        :param other: a Point or a Vector instance or a scalar number
        :return: a Point or a Vector instance
        """

        from basic_elements.Vector import Vector

        if isinstance(other, Vector):
            return Vector(self.x*other.x, self.y*other.y, self.z*other.z)

        if isinstance(other, Point):
            return Point(self.x*other.x, self.y*other.y, self.z*other.z)

        elif isinstance(other, int) or isinstance(other, float) or isinstance(other, long):
            return Point(self.x*other, self.y*other, self.z*other)


    def __abs__(self):

        """
        This function returns a Point composed using the absolute value for each coordinate
        :return: a Point instance
        """

        return Point(abs(self.x), abs(self.y), abs(self.z))


    def __div__(self, other):

        """
        This function implements the Point-scalar division component by component
        :param other: a scalar number
        :return: a Point instance
        """

        if isinstance(other, int) or isinstance(other, float) or isinstance(other, long):
            return Point(self.x/other, self.y/other, self.z/other)


    def __str__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Point('+str(self.x)+', '+str(self.y)+', '+str(self.z)+', '+str(self.w)+')'


    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Point('+str(self.x)+', '+str(self.y)+', '+str(self.z)+', '+str(self.w)+')'


    def __eq__(self, other):

        """
        This function returns a boolean value which indicates if given another Point, it is
        equal to this.
        :param other: a Point instance
        :return: a boolean value
        """

        return self.x == other.x and self.y == other.y and self.z == other.z


    def __ne__(self, other):

        """
        This function returns a boolean value which indicates if given another Point, it is not
        equal to this.
        :param other: a Point instance
        :return: a boolean value
        """

        return self.x != other.x and self.y != other.y and self.z != other.z
