__author__ = 'Francesco'

import math


class Vector:

    def __init__(self, x, y, z, *args):

        """
        This class models a 3d Vector expressed in homogeneous coordinates (so that it has
        4 components) by taking as input the following parameters:
        :param x: a float which stands for the x coordinate in a given coordinate system
        :param y: a float which stands for the y coordinate in a given coordinate system
        :param z: a float which stands for the z coordinate in a given coordinate system
        :param args: a float which stands for the last coordinate (optional)
        :return:a Vector instance
        """

        self.x = x
        self.y = y
        self.z = z
        self.w = 0
        if len(args) > 0:
            self.w = args[0]


    def __call__(self, *args, **kwargs):

        return self


    def __neg__(self):

        """
        This function returns the vector whose direction is opposite to this
        :return: a Vector instance
        """

        self.x = -self.x
        self.y = -self.y
        self.z = -self.z
        self.w = -self.w

        return self


    def __add__(self, other):

        """
        This function implements the sum Vector-Vector component by component.
        :param other: a Vector instance or a scalar number
        :return: a Vector instance
        """

        return Vector(self.x+other.x, self.y+other.y, self.z+other.z)


    def __sub__(self, other):

        """
        This function implements the difference Vector-Vector or Vector-scalar, component by component.
        :param other: a Vector or Point instance or a scalar number
        :return: a Vector instance
        """

        from basic_elements.Point import Point

        if isinstance(other, Vector) or isinstance(other, Point):
            return Vector(self.x-other.x, self.y-other.y, self.z-other.z)

        elif isinstance(other, float):
            return Vector(self.x-other, self.y-other, self.z-other)


    def __mul__(self, other):

        """
        This function implements the product Vector-Vector or Vector-scalar, component by component.
        :param other: a Vector instance or a scalar number
        :return: a Vector instance
        """

        if isinstance(other, Vector):
            return Vector(self.x*other.x, self.y*other.y, self.z*other.z)

        elif isinstance(other, float) or isinstance(other, long) or isinstance(other, int):
            return Vector(self.x*other, self.y*other, self.z*other)


    def __div__(self, other):

        """
        This function implements the division Vector-Vector or Vector-scalar, component by component.
        :param other: a Vector instance or a scalar number
        :return: a Vector instance
        """

        if isinstance(other, Vector):
            return Vector(self.x/other.x, self.y/other.y, self.z/other.y)
        elif isinstance(other, float):
            return Vector(self.x/other, self.y/other, self.z/other)


    def dot(self, other):

        """
        This function implements the dot product between 2 instances of Vector
        :param other: a Vector instance
        :return: a float number representing the dot product
        """

        x = self.x*other.x
        y = self.y*other.y
        z = self.z*other.z
        w = self.w*other.w

        return x+y+z+w


    def cross_p(self, other):

        """
        This function implements the cross product between 2 instances of Vector
        :param other: a Vector instance
        :return: a Vector instance, resulting from the cross product
        """

        x = self.y*other.z-other.y*self.z
        y = other.x*self.z-self.x*other.z
        z = self.x*other.y - other.x*self.y
        return Vector(x, y, z)


    def __str__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Vector('+str(self.x)+', '+str(self.y)+', '+str(self.z)+', '+str(self.w)+')'


    def __repr__(self):

        """
        :return: string representation of the current object for debugging purposes
        """

        return 'Vector('+self.x+', '+self.y+', '+self.z+', '+self.w+')'


    def __getitem__(self, item):

        return self


    def normalize(self):

        """
        This function produces the normalized Vector starting from the current instance
        :return: a Vector instance
        """

        norma = math.sqrt(self.x**2 + self.y**2 + self.z**2)
        if norma > 0:
            normalized = Vector(self.x/norma, self.y/norma, self.z/norma)
            return normalized
        else:
            return self