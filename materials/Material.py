__author__ = 'Francesco'


class Material:

    def __init__(self, ka, ke, kd, ks, sh, kt, kgt, kgts, kr, kgr, kgrs, ior, color, texture, bmap):

        """
        This class models the Material object. A material is defined through a set of coefficients which determine
        how light interacts with the material itself.
        :param ka: a float number, is the ambient light coefficient
        :param ke: a float number, is the emissive light coefficient
        :param kd: a float number, is the diffusive light coefficient
        :param ks: a float number, is the specular light coefficient
        :param sh: a float number, is the shiningness of the material used for the specular light
        :param kt: a float number, is the transmissive coefficient
        :param kgt: a float number, is the glossy transmissive coefficient
        :param kgts: a number, is the number of samples to adopt when the material is glossy transmissive
        :param kr: a float number, is the reflective coefficient
        :param kgr: a float number, is the glossy reflective coefficient
        :param kgrs: a number, is the number of samples to adopt when the material is glossy reflective
        :param ior: a float number, is the index of refraction to adopt when the material is transmissive or glossy transmissive
        :param color: a List containing the r, g, b values which determine the color of the material
        :param texture: a Texture object instance. When this parameter is set to be different from None,
                        then color parameter is discarded
        :param bmap: a BumpMap object instance. When this parameter is set to be different from None, the geometric
                     normal to the object's surface is perturbed according to the r, g, b values of the normal map.
        :return: a Material instance object
        """

        self.ka = ka
        self.ke = ke
        self.kd = kd
        self.ks = ks
        self.sh = sh
        self.color = color
        self.kt = kt
        self.kgt = kgt
        self.kgts = kgts
        self.kr = kr
        self.kgr = kgr
        self.kgrs = kgrs
        self.ior = ior
        self.texture = texture
        self.bmap = bmap








