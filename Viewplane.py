__author__ = 'Francesco'

class Viewplane:

    def __init__(self, width, height, distance):

        """
        This class models the Viewplane object.
        :param width: a number which specifies desired viewplane's width
        :param height: a number which specifies desired viewplane's height
        :param distance: a number which specifies at which distance the viewplane is located
                        from the camera
        :return: a Viewplane object instance
        """

        self.width = width
        self.height = height
        self.aspect = float(width/float(height))
        self.distance = distance
