__author__ = 'Francesco'
import cv2
from numpy import clip

class Cubemap:

    def __init__(self, filename):

        """
        This class models a Cubemap object used for the environment mapping (skybox).
        The format accepted for the input image is an horizontal cross with the right row longer --> -|---
        :param filename: the relative path of the image file to use
        :return: a Cubemap object instance
        """

        self.img = cv2.imread(filename)
        H = self.img.shape[0]
        W = self.img.shape[1]

        w = int(W/float(4))
        h = int(H/float(3))

        '''In the following the image structure is img[y, x]'''
        self.top = self.img[0:h-1, w:2*w-1]
        self.down = self.img[2*h+1:, w:2*w-1]
        self.right = self.img[h:2*h-1, 2*w:3*w-1]
        self.left = self.img[h:2*h-1, 0:w-1]
        self.front = self.img[h:2*h-1, w:2*w-1]
        self.back = self.img[h:2*h-1, 3*w:4*w-1]

    def readCubeMap(self, raydir):

        """
        This function calculates the u,v coordinates in tangent space by checking only the direction of the ray.
        By checking the direction is possible to determine which face of the cubemap we are looking at and this
        information is used to read from the appropriate face on the texture. Then the color of the texture is returned.
        :param raydir: the direction of the ray
        :return: the color of the image provided, at pixel (u,v)
        """

        dx = raydir.x
        dy = raydir.y
        dz = raydir.z
        max_d = max(abs(dx), abs(dy), abs(dz))
        face = None
        u = 0
        v = 0
        if abs(dx) == max_d:
            u = (dz/dx+1.0)*0.5
            v = 1.0 - (dy/dx+1.0)*0.5 if dx > 0.0 else (dy/dx+1.0)*0.5
            face = self.back if dx > 0.0 else self.front
        elif abs(dy) == max_d:
            v = 1.0 - (dx/dy+1.0)*0.5
            u = 1.0 - (dz/dy+1.0)*0.5 if dy > 0.0 else (dz/dy+1.0)*0.5
            face = self.top if dy > 0.0 else self.down
        elif abs(dz) == max_d:
            u = 1.0 - (dx/dz+1.0)*0.5
            v = 1.0 - (dy/dz+1.0)*0.5 if dz > 0.0 else (dy/dz+1.0)*0.5
            face = self.left if dz > 0.0 else self.right
        color = self.readTexture(face, u, v)
        return color


    def readTexture(self, face, u, v):

        """
        This function calculates the coordinates on the texture on the basis of the u,v coordinates and the face.
        Since u,v coordinates are in the range [0,1] it is necessary to compute the position on the image based
        on the image dimensions. The returned value is the r,g,b values at the [y,x] texel.
        :param face: the face of the cubemap on which we want to read from.
        :param u:
        :param v:
        :return: [r, g, b] value of the texel.
        """

        b, g, r = cv2.split(face)
        u = abs(u)
        v = abs(v)

        x = u*face.shape[1]
        y = v*face.shape[0]

        x = clip([x], 0.0, face.shape[1]-1)[0]
        y = clip([y], 0.0, face.shape[0]-1)[0]

        color_r = r[y, x]
        color_g = g[y, x]
        color_b = b[y, x]

        return [color_r, color_g, color_b]



