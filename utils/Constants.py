__author__ = 'Francesco'

import numpy as np
from basic_elements.Point import Point

"""
This file contains only some useful constants, widely used
- kEpsilon: this is needed to avoid the problem of self intersection
- inf_point: is an instance of Point where all of the components are set to infinite
"""
global kEpsilon
global inf_point


inf_point = Point(np.inf, np.inf, np.inf)
kEpsilon = 0.000001
