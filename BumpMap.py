__author__ = 'Francesco'


from cv2 import imread, split
from numpy import clip


class BumpMap:

    def __init__(self, mapname, effect_factor, repeatfor):

        """
        This class models a BumpMap object. The parameters used are the following
        :param mapname: a string, the relative path of the image file.
        :param effect_factor: a float, which indicates how much visibile has to be the effect.
        :param repeatfor: a number, which indicates how many times the map has to be repeated. The number
                            stands for n-times in height and n-times in width
        :return: a BumpMap object
        """

        self.name = mapname
        self.factor = effect_factor
        self.map = imread(mapname)
        self.repeatfor = int(repeatfor)
        self.sizeV = self.map.shape[0]
        self.sizeU = self.map.shape[1]
        self.aspect = self.sizeU/float(self.sizeV)
        b, g, r = split(self.map)
        self.r = r
        self.g = g
        self.b = b


    def read(self, u, v):

        """
        This function reads the texel on the image associated to the BumpMap object, provided the coordinates in the
        tangent space (u, v). Since these coordinates are in range [0,1], the dimensions of the image are used to
        get the right pixel (by using an interpolation).
        :param u: a float number between 0 and 1
        :param v: a float number between 0 and 1
        :return: a List object containing the r, g, b values for the color of the texel
        """

        '''vertical coordinate v has to be taken upside-down because of the way images are accessed'''
        x = abs(u)*self.sizeU
        y = (1-abs(v))*self.sizeV

        '''
        if the texture must be repeated, we have to start reading again from 0 when we overflow the max level for
        that dimension
        '''
        coeff = [i for i in range(1, self.repeatfor) if x > i*self.sizeU]
        coeff = coeff[len(coeff)-1] if len(coeff) > 0 \
                                    else 0

        tiled_x = x - coeff*self.sizeU
        x = tiled_x if tiled_x > 0 \
                    else x

        coeff = [i for i in range(0, self.repeatfor-1) if abs(y) > i*self.sizeV]
        coeff = coeff[len(coeff)-1] if len(coeff) > 0 \
                                    else 0

        tiled_y = abs(y) - coeff*self.sizeV
        y = tiled_y if y > 0 \
                    else (self.sizeV - tiled_y)

        x = clip([x], 0.0, self.sizeU-1)[0]
        y = clip([y], 0.0, self.sizeV-1)[0]

        color_r = self.r[y, x]
        color_g = self.g[y, x]
        color_b = self.b[y, x]

        return [color_r, color_g, color_b]